/******************
	This test provies scenario for smoke testing data imports to the transfer-service:
		1.- Assess the current performance of the transfer-service for basic benchmark tests.
		2.- Make sure that the transfer-service is continuously meeting the performance standards as changes are made to the system (code).
	
*******************/
import http from 'k6/http';
import { check, sleep, group, fail } from 'k6';
import { Rate, Trend } from 'k6/metrics';

//K6 cloud test name
let TEST_NAME = "smoke-test";
if(typeof __ENV.TEST_NAME !== 'undefined'){
	TEST_NAME = __ENV.TEST_NAME;
}

//Default transfer-service host url
let BASE_URL = "http://127.0.0.1:93";
if(typeof __ENV.TRANSFER_SERVICE_HOSTNAME !== 'undefined'){
	BASE_URL = __ENV.TRANSFER_SERVICE_HOSTNAME;
}
	
let DATASPACE="stable";
if(typeof __ENV.DATASPACE !== 'undefined'){
	DATASPACE = __ENV.DATASPACE;
}

//Keycloak parameters
let ACCESS_TOKEN_URL= "http://keycloak-staging-oecd.redpelicans.com/auth/realms/OECD/protocol/openid-connect/token";
if(typeof __ENV.ACCESS_TOKEN_URL !== 'undefined'){
	ACCESS_TOKEN_URL = __ENV.ACCESS_TOKEN_URL;
}
let currentAccessToken = "";
let accessTokenExpiry ="";

//Keycloak credentials
let USERNAME="";
if(typeof __ENV.USERNAME !== 'undefined'){
	USERNAME = __ENV.USERNAME;
}

let PASSWORD="";
if(typeof __ENV.PASSWORD !== 'undefined'){
	PASSWORD = __ENV.PASSWORD;
}

//login to keycloak?
let getToken =true;
if(typeof __ENV.USERNAME === 'undefined' || typeof __ENV.PASSWORD === 'undefined'){
	getToken=false;
}

let INPUT_FILE = "./Resources/test-cases.json";
if(typeof __ENV.TEST_CASES_FILE !== 'undefined'){
	INPUT_FILE = __ENV.TEST_CASES_FILE;
}

//Load test cases from json file
const TEST_CASES= JSON.parse(open(INPUT_FILE));
//Open input files
for(let testCase in TEST_CASES){
	if(TEST_CASES[testCase].format !=="sdmx")
		TEST_CASES[testCase].data = open(`./Resources/Data/${TEST_CASES[testCase].dataFile}`, "b");
	if(TEST_CASES[testCase].format ==="excel")
		TEST_CASES[testCase].edd = open(`./Resources/Data/${TEST_CASES[testCase].eddFile}`, "b");
}

//IMPORTANT! The time has to be adjusted depending on the amount of imports, to give enough time for all the imports to complete
let waitTimeInSeconds = TEST_CASES.length * 60;//total amount of usecases * 30 seconds

let importRate = new Rate('data_import_completed');
let importTrend = new Trend('data_import_time', true);

export let options = {
	setupTimeout: "10s",
	ext: {
		loadimpact: {
		  projectID: 3497348, //k6 CLOUD project id 
		  name: TEST_NAME
		}
	},
	thresholds: {
		"checks": ['rate>0.99'], // more than 99% success rate on import requests
		"data_import_completed": ['rate>0.90'], // more than 95% success rate of transactions imported
        "data_import_time{import_type:csv_small}": ["p(95)<10000"],//less than 10 seconds
        "data_import_time{import_type:csv_medium}": ["p(95)<150000"],//less than 2.5 minutes
        "data_import_time{import_type:xml_small}": ["p(95)<10000"],//less than 10 seconds
        "data_import_time{import_type:xml_medium}": ["p(95)<150000"],//less than 2.5 minutes
        "data_import_time{import_type:sdmx_small}": ["p(95)<10000"],//less than 10 seconds
        "data_import_time{import_type:sdmx_medium}": ["p(95)<150000"],//less than 2.5 minutes
        "data_import_time{import_type:excel_small}": ["p(95)<10000"],//less than 10 seconds
        "data_import_time{import_type:excel_medium}": ["p(95)<150000"],//less than 2.5 minutes
		
        "data_import_time{datasetSize:extraSmall}": ["p(95)<150000"],
        "data_import_time{datasetSize:small}": ["p(95)<150000"],
        "data_import_time{datasetSize:medium}": ["p(95)<150000"],
        "data_import_time{datasetSize:large}": ["p(95)<150000"],
        "data_import_time{datasetSize:extraLarge}": ["p(95)<150000"],	
	},
	iterations: 1,
	vus: 1,

};

export function setup() {
	// 2. setup code
	
	//check that the transfer-service hostname is available
	let healhCheck = http.get(`${BASE_URL}/health`);
	if (healhCheck.status !== 200){
		fail(`Error: the transfer-service {${BASE_URL}/health} is not responding.`);
	}
	console.log(`Testing the transfer-service {${BASE_URL}} version ${healhCheck.json().service.details.version}`);
	
}

export default function() {
	var submitedRequests = [];

	TryToGetNewAccessToken();
	//Submit data import requests
	for(let testCase in TEST_CASES){
		let headers= {
			'Accept':'application/json',
			'Authorization': `Bearer ${currentAccessToken}`, 
		};
		
		var method = "/1.2/import/sdmxFile";
		let data= { 
			'dataspace': DATASPACE	
		};
		
		//Import from SDMX source
		if(TEST_CASES[testCase].format ==="sdmx" ){
			data.filepath= TEST_CASES[testCase].sdmxSource;
		}
		//Import from Excel
		else if(TEST_CASES[testCase].format ==="excel" ){
		    method = "/1.2/import/excel";
			data.eddFile  = http.file(TEST_CASES[testCase].edd, TEST_CASES[testCase].eddFile);
			data.excelFile = http.file(TEST_CASES[testCase].data, TEST_CASES[testCase].dataFile);
		}
		//Import from CSV and XML
		else{
			data.file = http.file(TEST_CASES[testCase].data, TEST_CASES[testCase].dataFile);
		}
		
		var res = http.post(`${BASE_URL}${method}`, data, {headers: headers});

		console.log(`import status:${res.status}`);
		console.log(`import message:${res.json().message}`);
		
		check(res, {
			'is status 200': (r) => r.status === 200
		});
		 
		if(res.status ===200){
			var transactionID = res.json().message.match(/\d+/g);
			submitedRequests.push({
				'ID': transactionID,
				'format': TEST_CASES[testCase].format,
				'size': TEST_CASES[testCase].size,
				'datasetSize': TEST_CASES[testCase].datasetSize,
			});
		}
	}
	
	//Wait for the transfer-service to complete all imports
	sleep(waitTimeInSeconds);
	
	//Get new access token if current token has expired
	TryToGetNewAccessToken();
	
	//Get status of requests
	for(let request in submitedRequests){
		let data= { 
			'dataspace': DATASPACE,	
			'id': parseInt(submitedRequests[request].ID)
		};
		let headers= {
			'Accept':'application/json',
			'Authorization': 'Bearer ' + currentAccessToken, 
		};
		
		var res = http.post(
			`${BASE_URL}/1.2/status/request`, 
			data,  
			{headers: headers},
			{tags:{datasetSize:submitedRequests[request].datasetSize}},//tags
		);
			
		if(res.status===200){
			if(res.json().executionStatus==="Completed"){
				var actualTime = Date.parse(res.json().executionEnd) - Date.parse(res.json().executionStart);
				importTrend.add(actualTime, { import_type: `${submitedRequests[request].format}_${submitedRequests[request].size}` });
				importTrend.add(actualTime, { datasetSize: `${submitedRequests[request].datasetSize}` });
				
				//The import was completed
				importRate.add(true);
			}
			else{
				console.log(`Execution status:${res.json().executionStatus}`);
				//The import was not completed
				importRate.add(false);
			}
		
		}
	}
}

function TryToGetNewAccessToken(){
	
    if (!accessTokenExpiry || !currentAccessToken) {
        console.log("Token or expiry date are missing");
    } else if (accessTokenExpiry <= new Date().getTime()) {
        console.log("Token is expired");
    } else {
        getToken = false;
        console.log("Token and expiry date are all good");
    }

    if (getToken === true) {
        //get the access token first
		let data= { 
			'grant_type': "password", 
			'client_id': "app", 
			'scope': "openid", 
			'username': USERNAME, 
			'password': PASSWORD
		};
		//Get new access token
		let res = http.post(ACCESS_TOKEN_URL, data, {headers: {'Accept':'application/json'}});
		
        var responseJson = res.json();
        currentAccessToken = responseJson.access_token;
        var expiryDate = new Date();
        expiryDate.setSeconds(
          expiryDate.getSeconds() + responseJson.expires_in
        );
        accessTokenExpiry = expiryDate.getTime();
       
        sleep(1);
        //console.log(currentAccessToken);
    }
	
	if(getToken && !currentAccessToken)
		fail("Could not login, please check the USERNAME and PASSWORD");
}
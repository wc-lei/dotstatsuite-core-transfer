﻿using DotStat.Common.Configuration.Dto;
using DotStat.Common.Localization;
using DotStat.Test.Moq;
using DotStat.Transfer.Param;
using DotStat.Transfer.Producer;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
using System.Globalization;

namespace DotStat.Transfer.Test.Unit.Producer
{
    [TestFixture]
    public class SdmxFilesProducerTests
    {
        [TestCase("sdmx/264D_264_SALDI+2.1.xml", "data/264D_264_SALDI_compact.xml",12)]
        [TestCase("sdmx/264D_264_SALDI+2.1.xml", "data/264D_264_SALDI_generic.xml",12)]
        [TestCase("sdmx/264D_264_SALDI+2.1.xml", "data/264D_264_SALDI.csv", 101)]
        [TestCase("sdmx/DF_DROPOUT_RT.xml", "data/DF_DROPOUT_RT_generic_series.xml", 810)]
        [TestCase("sdmx/DF_DROPOUT_RT.xml", "data/DF_DROPOUT_RT_generic_flat.xml", 810)]
        [TestCase("sdmx/DF_DROPOUT_RT.xml", "data/DF_DROPOUT_RT_generic_flat.zip", 810)]
        public void Test_SdmxFile_Read_Dataflow_Reference(string structureFile, string dataFile, int expectedObservations)
        {
            var producer = new SdmxFileProducer(new TestMappingStoreDataAccess(structureFile));

            var param = new SdmxFileToSqlTransferParam()
            {
                DestinationDataspace = new DataspaceInternal() { Id = "Test"}, //dummy
                SourceDataflow = new DataflowMutableCore(), //dummy
                FilePath = dataFile
            };

            var dataflow = producer.GetDataflow(param);
            var transferContent = producer.Process(param, dataflow);

            Assert.IsNotNull(transferContent.Observations);

            var observations        = 0;

            using (var enumerator = transferContent.Observations.GetEnumerator())
                while (enumerator.MoveNext())
                    observations++;

            Assert.AreEqual(expectedObservations, observations);
        }
    }
}

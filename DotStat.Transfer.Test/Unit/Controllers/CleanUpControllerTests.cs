﻿using DotStat.Common.Auth;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Dto;
using DotStat.Db.Manager;
using DotStat.Db.Repository;
using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Test;
using DotStatServices.Transfer;
using DotStatServices.Transfer.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
using Org.Sdmxsource.Sdmx.Util.Extension;
using Org.Sdmxsource.Sdmx.Util.Objects.Container;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using DotStat.Common.Enums;
using DotStat.Db.Dto;
using DotStat.Transfer.Manager;
using Estat.Sdmxsource.Extension.Constant;

namespace DotStat.Transfer.Test.Unit.Controllers
{
    [TestFixture]
    public class CleanUpControllerTests : SdmxUnitTestBase
    {
        private CleanUpController _controller;
        private readonly AuthConfiguration _authConfig;
        private readonly Mock<IHttpContextAccessor> _contextAccessorMock = new Mock<IHttpContextAccessor>();
        private readonly Mock<IAuthorizationManagement> _authManagementMock = new Mock<IAuthorizationManagement>();
        private readonly string _dataSpace = "design";
        private readonly Mock<IMappingStoreDataAccess> _mappingStoreDataAccessMock = new Mock<IMappingStoreDataAccess>();
        private readonly Mock<IDbManager> _dbManagerMock = new Mock<IDbManager>();
        private readonly Mock<IManagementRepository> _managementRepositoryMock = new Mock<IManagementRepository>();
        private readonly Mock<ITransactionRepository> _transactionRepositoryMock = new Mock<ITransactionRepository>();
        private readonly CommonManager _commonManager;
        private readonly SdmxObjectsImpl _sdmxObj;

        public CleanUpControllerTests()
        {
            _contextAccessorMock
                .Setup(x => x.HttpContext.User)
                .Returns(new ClaimsPrincipal(new ClaimsIdentity()));

            _authConfig = new AuthConfiguration()
            {
                ClaimsMapping = new Dictionary<string, string>()
            };

            _sdmxObj = new SdmxObjectsImpl(DatasetActionEnumType.Append);
            _sdmxObj.Merge(new FileInfo("sdmx/264D_264_SALDI+2.1.xml").GetSdmxObjects(new StructureParsingManager()));
            _sdmxObj.Merge(new FileInfo("sdmx/ESTAT+NA.xml").GetSdmxObjects(new StructureParsingManager()));
            
            foreach (var dsd in _sdmxObj.DataStructures)
            {
                _authManagementMock
                    .Setup(x => x.IsAuthorized(
                        It.IsAny<DotStatPrincipal>(),
                        _dataSpace,
                        dsd.AgencyId,
                        dsd.Id,
                        dsd.Version,
                        PermissionType.CanDeleteStructuralMetadata
                    ))
                    .Returns(true);
            }

            _dbManagerMock
                .Setup(x => x.GetManagementRepository(It.IsAny<string>()))
                .Returns(_managementRepositoryMock.Object);

            _dbManagerMock
                .Setup(x => x.GetTransactionRepository(It.IsAny<string>()))
                .Returns(_transactionRepositoryMock.Object);

            _commonManager = new CommonManager(_mappingStoreDataAccessMock.Object, _authManagementMock.Object,
                Configuration);

            Configuration.SpacesInternal = new List<DataspaceInternal>()
            {
                new DataspaceInternal() {Id = _dataSpace}
            };

            Configuration.DefaultLanguageCode = "en";
        }

        [Test]
        public void CleanUpDsdWithWrongDsd()
        {
            _controller = new CleanUpController(_contextAccessorMock.Object, _authConfig, Configuration, _dbManagerMock.Object, _commonManager);

            var exception = Assert.Throws<ArgumentException>(() => _controller.CleanUpDsd("design", "wrong ID"));
            Assert.That(exception.Message.Contains("AGENCYID:ID(VERSION)", StringComparison.OrdinalIgnoreCase));
        }

        [Test]
        public void CleanUpDsdUnauthorized()
        {
            _controller = new CleanUpController(_contextAccessorMock.Object, _authConfig, Configuration, _dbManagerMock.Object, _commonManager);

            var result = _controller.CleanUpDsd("design", "NOACCESS:264D_Consumer_Confiance(1.0)");
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Result);
            Assert.IsInstanceOf<ForbidResult>(result.Result);
        }

        [Test]
        public void CleanUpDsdMappingStoreHasDsd()
        {
            SetupMappingStoreDataAccess(new SdmxObjectsImpl(null, _sdmxObj.DataStructures));

            _controller = new CleanUpController(_contextAccessorMock.Object, _authConfig, Configuration, _dbManagerMock.Object, _commonManager);

            var result = _controller.CleanUpDsd("design", "IT1:264D_Consumer_Confiance(1.0)");
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Value);
            Assert.IsInstanceOf<OperationResult>(result.Value);
            Assert.IsFalse(result.Value.Success);
        }

        [Test]
        public void CleanUpDsdNothingToDelete()
        {
            SetupMappingStoreDataAccess(new SdmxObjectsImpl());
            SetupManagementRepository(false, -1);
            SetupTransactionRepository(true);

            _controller = new CleanUpController(_contextAccessorMock.Object, _authConfig, Configuration, _dbManagerMock.Object, _commonManager);

            var result = _controller.CleanUpDsd("design", "IT1:264D_Consumer_Confiance(1.0)");
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Value);
            Assert.IsInstanceOf<OperationResult>(result.Value);
            Assert.IsFalse(result.Value.Success);
        }

        [Test]
        public void CleanUpDsdSuccessful()
        {
            SetupMappingStoreDataAccess(new SdmxObjectsImpl());
            SetupManagementRepository(true, 1);
            SetupTransactionRepository(true);

            _controller = new CleanUpController(_contextAccessorMock.Object, _authConfig, Configuration, _dbManagerMock.Object, _commonManager);

            var result = _controller.CleanUpDsd("design", "IT1:264D_Consumer_Confiance(1.0)");
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Value);
            Assert.IsInstanceOf<OperationResult>(result.Value);
            Assert.IsTrue(result.Value.Success);
        }

        [Test]
        public void CleanUpDsdConflictingTransaction()
        {
            SetupMappingStoreDataAccess(new SdmxObjectsImpl());
            SetupManagementRepository(true, 1);
            SetupTransactionRepository(false);

            _controller = new CleanUpController(_contextAccessorMock.Object, _authConfig, Configuration, _dbManagerMock.Object, _commonManager);

            var result = _controller.CleanUpDsd("design", "IT1:264D_Consumer_Confiance(1.0)");
            Assert.IsNotNull(result);
            Assert.IsInstanceOf<ConflictObjectResult>(result.Result);
            var conflicResult = (ConflictObjectResult) result.Result;

            Assert.IsNotNull(conflicResult.Value);
            Assert.IsInstanceOf<OperationResult>(conflicResult.Value);
            Assert.IsFalse(((OperationResult)conflicResult.Value).Success);
        }

        [Test]
        public void CleanUpOrphanDsd()
        {
            SetupMappingStoreDataAccess(new SdmxObjectsImpl());

            SetupManagementRepository(true, 1);
            SetupTransactionRepository(true);

            _controller = new CleanUpController(_contextAccessorMock.Object, _authConfig, Configuration, _dbManagerMock.Object, _commonManager);
            var result = _controller.CleanUpOrphanDsds("design");

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Value);
            Assert.IsInstanceOf<OperationResult>(result.Value);
            Assert.IsTrue(result.Value.Success);
        }

        [Test]
        public void CleanUpNonOrphanDsds()
        {
            SetupMappingStoreDataAccess(_sdmxObj);
            
            SetupManagementRepository(true, 1);
            SetupTransactionRepository(true);
            
            _controller = new CleanUpController(_contextAccessorMock.Object, _authConfig, Configuration, _dbManagerMock.Object, _commonManager);
            var result = _controller.CleanUpOrphanDsds("design");

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Value);
            Assert.IsInstanceOf<OperationResult>(result.Value);
            Assert.IsTrue(result.Value.Success);
        }

        [Test]
        public void CleanUpOrphanDsdsWithConcurrentTransaction()
        {
            SetupMappingStoreDataAccess(new SdmxObjectsImpl());
            SetupManagementRepository(true, 1);
            SetupTransactionRepository(false);
            
            _controller = new CleanUpController(_contextAccessorMock.Object, _authConfig, Configuration, _dbManagerMock.Object, _commonManager);
            var result = _controller.CleanUpOrphanDsds("design");
            
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Result);
            Assert.IsInstanceOf<ConflictObjectResult>(result.Result);

            var objResult = (ConflictObjectResult)result.Result;
            Assert.IsInstanceOf<OperationResult>(objResult.Value);
            Assert.IsFalse(((OperationResult)objResult.Value).Success);
        }

        [Test]
        public void CleanUpOrphanDsdsWithStatus200And409()
        {
            SetupMappingStoreDataAccess(new SdmxObjectsImpl());
            SetupManagementRepository(true, 1);
            SetupTransactionRepository(_sdmxObj);

            _controller = new CleanUpController(_contextAccessorMock.Object, _authConfig, Configuration, _dbManagerMock.Object, _commonManager);
            var result = _controller.CleanUpOrphanDsds("design");

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Result);
            Assert.IsInstanceOf<ObjectResult>(result.Result);

            ObjectResult objResult = (ObjectResult)result.Result;
            Assert.IsInstanceOf<ObjectResult[]>(objResult.Value);
            ObjectResult[] objResults = (ObjectResult[]) objResult.Value;
            Assert.IsInstanceOf<OkObjectResult>(objResults[0]);
            Assert.IsInstanceOf<ConflictObjectResult>(objResults[1]);
        }

        [TearDown]
        public void TearDown()
        {
            _mappingStoreDataAccessMock.Invocations.Clear();
            _managementRepositoryMock.Invocations.Clear();
            _transactionRepositoryMock.Invocations.Clear();
        }

        private void SetupMappingStoreDataAccess(ISdmxObjects sdmxObjs)
        {
            _mappingStoreDataAccessMock.Setup(x =>
                    x.GetDsd(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(sdmxObjs);
        }

        private void SetupManagementRepository(bool cleanUpDsdResult, int resultOfGetArtefactDbId)
        {
            _managementRepositoryMock
                .Setup(x => x.CleanUpDsd(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<SdmxVersion>()))
                .Returns(cleanUpDsdResult);

            _managementRepositoryMock
                .Setup(x => x.GetArtefactDbId(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<SdmxVersion>(),
                    It.IsAny<SDMXArtefactType>())).Returns(resultOfGetArtefactDbId);

            _managementRepositoryMock
                .Setup(x => x.GetListOfArtefacts("DSD")).Returns(
                    _sdmxObj.DataStructures.Select(dsd => new ArtefactItem() {Agency = dsd.AgencyId, Id = dsd.Id, Version = dsd.Version})
                        .ToList()
                );
        }

        private void SetupTransactionRepository(bool resultOfTryNewTransaction)
        {
            _transactionRepositoryMock.Setup(x =>
                x.TryNewTransactionForCleanup(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<SdmxVersion>(), It.IsAny<DotStatPrincipal>()))
                .Returns(resultOfTryNewTransaction);
        }

        private void SetupTransactionRepository(ISdmxObjects sdmxObjs)
        {
            bool returnValue = true;

            foreach (var dsd in sdmxObjs.DataStructures)
            {
                _transactionRepositoryMock.Setup(x =>
                    x.TryNewTransactionForCleanup(
                        It.IsAny<int>(),
                        It.Is<string>(i => i == dsd.AgencyId),
                        It.Is<string>(i => i == dsd.Id),
                        It.IsAny<SdmxVersion>(), 
                        It.IsAny<DotStatPrincipal>()
                    )).Returns(returnValue);

                returnValue = !returnValue;
            }
            
        }


    }
}

﻿using DotStat.Common.Auth;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Dto;
using DotStat.Db.Manager;
using DotStat.Db.Repository;
using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Test;
using DotStatServices.Transfer;
using DotStatServices.Transfer.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Net;
using System.Security.Claims;
using DotStat.Common.Localization;
using DotStat.Transfer.Manager;
using Estat.Sdmxsource.Extension.Constant;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

namespace DotStat.Transfer.Test.Unit.Controllers
{
    [TestFixture]
    public class DataManagementControllerTests : SdmxUnitTestBase
    {
        private readonly Dataflow _dataflow;
        private readonly Mock<IHttpContextAccessor> _contextAccessorMock = new Mock<IHttpContextAccessor>();
        private readonly Mock<IAuthorizationManagement> _authManagementMock = new Mock<IAuthorizationManagement>();
        private const string ResetDataSpace = "reset";
        private const string StableDataSpace = "stable";

        private readonly Mock<IMappingStoreDataAccess>
            _mappingStoreDataAccessMock = new Mock<IMappingStoreDataAccess>();

        private readonly Mock<IDbManager> _dbManagerMock = new Mock<IDbManager>();
        private readonly Mock<IManagementRepository> _managementRepositoryMock = new Mock<IManagementRepository>();
        private readonly Mock<ITransactionRepository> _transactionRepositoryMock = new Mock<ITransactionRepository>();

        private readonly AuthConfiguration _authConfig;
        private readonly CommonManager _commonManager;

        public DataManagementControllerTests()
        {
            _contextAccessorMock
                .Setup(x => x.HttpContext.User)
                .Returns(new ClaimsPrincipal(new ClaimsIdentity()));

            _authConfig = new AuthConfiguration()
            {
                ClaimsMapping = new Dictionary<string, string>()
            };

            _dataflow = GetDataflow();

            SetupAuthorizationManagement(ResetDataSpace, PermissionType.CanImportData);

            SetupMappingStoreDataAccess(_dataflow);

            _dbManagerMock
                .Setup(m => m.GetManagementRepository(It.IsAny<string>()))
                .Returns(_managementRepositoryMock.Object);

            _dbManagerMock
                .Setup(m => m.GetTransactionRepository(It.IsAny<string>()))
                .Returns(_transactionRepositoryMock.Object);

            Configuration.SpacesInternal = new List<DataspaceInternal>()
            {
                new DataspaceInternal() {Id = ResetDataSpace},
                new DataspaceInternal() {Id = StableDataSpace}
            };

            Configuration.DefaultLanguageCode = "en";

            _commonManager = new CommonManager(_mappingStoreDataAccessMock.Object, _authManagementMock.Object,
                Configuration);
        }

        [Test]
        public void InitDatabaseObjectsOfDataflowInvalidDataflow()
        {
            var controller = new DataManagementController(_contextAccessorMock.Object, _authConfig,
                _authManagementMock.Object, _mappingStoreDataAccessMock.Object, Configuration, _dbManagerMock.Object,
                _commonManager);

            var exception = Assert.Throws<ArgumentException>(() =>
                controller.InitDatabaseObjectsOfDataflow(ResetDataSpace, "NOAGENCY:NOID(1.0)"));

            StringAssert.AreEqualIgnoringCase(
                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DataflowNotFoundInDataspace),
                exception.Message);
        }

        [Test]
        public void InitDatabaseObjectsOfDataflowForbiddenAccess()
        {
            var controller = new DataManagementController(_contextAccessorMock.Object, _authConfig,
                _authManagementMock.Object, _mappingStoreDataAccessMock.Object, Configuration, _dbManagerMock.Object,
                _commonManager);

            var result = controller.InitDatabaseObjectsOfDataflow(StableDataSpace, _dataflow.FullId);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Result);
            Assert.IsInstanceOf<ForbidResult>(result.Result);
        }

        [Test]
        public void InitDatabaseObjectsOfDataflowSuccessAlreadyExists()
        {
            SetupManagementRepository(DataflowInDataDatabase.Exists);
            SetupTransactionRepository(ConcurrentTransactionState.NotExists);

            var controller = new DataManagementController(_contextAccessorMock.Object, _authConfig,
                _authManagementMock.Object, _mappingStoreDataAccessMock.Object, Configuration, _dbManagerMock.Object,
                _commonManager);

            var result = controller.InitDatabaseObjectsOfDataflow(ResetDataSpace, _dataflow.FullId);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Result);
            Assert.IsInstanceOf<OkObjectResult>(result.Result);

            var okObjectResult = (OkObjectResult) result.Result;
            Assert.AreEqual((int)HttpStatusCode.OK, okObjectResult.StatusCode);

            Assert.IsInstanceOf<OperationResult>(okObjectResult.Value);
            Assert.IsTrue(((OperationResult)okObjectResult.Value).Success);
        }

        [Test]
        public void InitDatabaseObjectsOfDataflowSuccessCreated()
        {
            SetupManagementRepository(DataflowInDataDatabase.NotExists);
            SetupTransactionRepository(ConcurrentTransactionState.NotExists);

            var controller = new DataManagementController(_contextAccessorMock.Object, _authConfig,
                _authManagementMock.Object, _mappingStoreDataAccessMock.Object, Configuration, _dbManagerMock.Object,
                _commonManager);

            var result = controller.InitDatabaseObjectsOfDataflow(ResetDataSpace, _dataflow.FullId);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Result);
            Assert.IsInstanceOf<CreatedResult>(result.Result);

            var createdObjectResult = (CreatedResult)result.Result;
            Assert.AreEqual((int)HttpStatusCode.Created, createdObjectResult.StatusCode);

            Assert.IsInstanceOf<OperationResult>(createdObjectResult.Value);
            Assert.IsTrue(((OperationResult)createdObjectResult.Value).Success);
        }

        [Test]
        public void InitDatabaseObjectsOfDataflowConcurrentTransaction()
        {
            SetupManagementRepository(DataflowInDataDatabase.NotExists);
            SetupTransactionRepository(ConcurrentTransactionState.Exists);

            var controller = new DataManagementController(_contextAccessorMock.Object, _authConfig,
                _authManagementMock.Object, _mappingStoreDataAccessMock.Object, Configuration, _dbManagerMock.Object,
                _commonManager);

            var result = controller.InitDatabaseObjectsOfDataflow(ResetDataSpace, _dataflow.FullId);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Result);
            Assert.IsInstanceOf<ConflictObjectResult>(result.Result);

            var conflictObjectResult = (ConflictObjectResult)result.Result;
            Assert.AreEqual((int)HttpStatusCode.Conflict, conflictObjectResult.StatusCode);

            Assert.IsInstanceOf<OperationResult>(conflictObjectResult.Value);
            Assert.IsFalse(((OperationResult)conflictObjectResult.Value).Success);
        }

        [TearDown]
        public void TearDown()
        {
            _authManagementMock.Invocations.Clear();
            _managementRepositoryMock.Invocations.Clear();
            _transactionRepositoryMock.Invocations.Clear();
        }

        private void SetupMappingStoreDataAccess(Dataflow dataflow = null)
        {
            var df = dataflow ?? _dataflow;

            _mappingStoreDataAccessMock
                .Setup(m => m.GetDataflow(
                    ResetDataSpace,
                    df.AgencyId,
                    df.Code,
                    df.Version.ToString(),
                    true
                ))
                .Returns(df);

            _mappingStoreDataAccessMock
                .Setup(m => m.GetDataflow(
                    ResetDataSpace,
                    It.IsAny<string>(),
                    It.IsNotIn(df.Code),
                    It.IsAny<string>(),
                    true
                ))
                .Throws(new ArgumentException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DataflowNotFoundInDataspace)));
        }

        private void SetupManagementRepository(DataflowInDataDatabase dataflowExistsInDataDb)
        {
            _managementRepositoryMock
                .Setup(m => m.GetArtefactDbId(It.IsNotNull<IMaintainableObject>()))
                .Returns(() => dataflowExistsInDataDb == DataflowInDataDatabase.Exists ? 1 : -1);
        }

        private void SetupTransactionRepository(ConcurrentTransactionState concurrentTransactionExists )
        {
            _transactionRepositoryMock
                .Setup(m => m.TryNewTransaction(It.IsAny<int>(), _dataflow, TargetVersion.Live, false, It.IsAny<DotStatPrincipal>()))
                .Returns(() => concurrentTransactionExists == ConcurrentTransactionState.NotExists);
        }

        private void SetupAuthorizationManagement(string allowedDataSpace, PermissionType testedPermission)
        {
            _authManagementMock
                .Setup(x => x.IsAuthorized(
                    It.IsAny<DotStatPrincipal>(),
                    allowedDataSpace,
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    testedPermission
                ))
                .Returns(true);

            _authManagementMock
                .Setup(x => x.IsAuthorized(
                    It.IsAny<DotStatPrincipal>(),
                    It.IsNotIn(allowedDataSpace),
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<PermissionType>()
                ))
                .Returns(false);
        }

        private enum DataflowInDataDatabase
        {
            Exists,
            NotExists
        }

        private enum ConcurrentTransactionState
        {
            Exists,
            NotExists
        }
    }
}

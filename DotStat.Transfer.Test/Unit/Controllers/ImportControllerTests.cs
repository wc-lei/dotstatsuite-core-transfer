﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Auth;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Dto;
using DotStat.Db.Manager;
using DotStat.Db.Repository;
using DotStat.Domain;
using DotStat.Test;
using DotStat.Test.Moq;
using DotStat.Transfer.Consumer;
using DotStat.Transfer.Manager;
using DotStat.Transfer.Param;
using DotStat.Transfer.Producer;
using DotStatServices.Transfer;
using DotStatServices.Transfer.BackgroundJob;
using DotStatServices.Transfer.Controllers;
using DotStatServices.Transfer.Services;
using Estat.Sdmxsource.Extension.Constant;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace DotStat.Transfer.Test.Unit.Controllers
{
    [TestFixture]
    public class ImportControllerTests : UnitTestBase
    {
        private readonly Mock<IManagementRepository> _mgmtRepositoryMock = new Mock<IManagementRepository>();
        private readonly Mock<IDbManager> _dbManagerMock = new Mock<IDbManager>();
        private readonly Mock<IHttpContextAccessor> _contextAccessorMock = new Mock<IHttpContextAccessor>();
        private readonly Mock<IAuthorizationManagement> _authManagementMock = new Mock<IAuthorizationManagement>();

        private readonly Mock<IConsumer<TransferParam>> _consumerMock = new Mock<IConsumer<TransferParam>>();

        private readonly Mock<IMailService> _mailServiceMock = new Mock<IMailService>();
        private readonly Mock<BackgroundQueue> _backgroundQueueMock = new Mock<BackgroundQueue>();
        private readonly ImportController _controller;
        private readonly AuthConfiguration _authConfig;

        private readonly string _dataSpace = "design";
        private readonly Dataflow _dataflow;
        private int _obsCount;

        public ImportControllerTests()
        {
            this.Configuration.SpacesInternal = new List<DataspaceInternal>()
            {
                new DataspaceInternal() {Id = _dataSpace}
            };

            _authConfig = new AuthConfiguration()
            {
                ClaimsMapping = new Dictionary<string, string>()
            };

            // ----------------------------------------------------------------------

            var mappingStore = new TestMappingStoreDataAccess("sdmx/264D_264_SALDI+2.1.xml");
            _dataflow = mappingStore.GetDataflow();

            // Setup required moq objects -------------------------------------------

            _dbManagerMock
                .Setup(x => x.GetManagementRepository(It.IsAny<string>()))
                .Returns(_mgmtRepositoryMock.Object);

            _contextAccessorMock
                .Setup(x => x.HttpContext.User)
                .Returns(new ClaimsPrincipal(new ClaimsIdentity()));

            _authManagementMock
                .Setup(x => x.IsAuthorized(
                    It.IsAny<DotStatPrincipal>(),
                    _dataSpace,
                    _dataflow.AgencyId,
                    _dataflow.Code,
                    _dataflow.Version.ToString(),
                    PermissionType.CanImportData
                ))
                .Returns(true);

            _consumerMock
                .Setup(x => x.IsAuthorized(
                    It.IsAny<TransferParam>(),
                    It.IsAny<Dataflow>()
                ))
                .Returns(true);

            _consumerMock
                .Setup(x => x.Save(
                    It.IsAny<TransferParam>(),
                    It.IsAny<Dataflow>(),
                    It.IsAny<TransferContent>()
                ))
                .Returns(true)
                .Callback((TransferParam p, Dataflow d, TransferContent c) => { _obsCount = c.Observations.Count(); });

            _backgroundQueueMock
                .Setup(x => x.Enqueue(It.IsAny<Func<CancellationToken, Task>>()))
                .Callback((Func<CancellationToken, Task> task) =>
                {
                    task.Invoke(new CancellationToken());
                });

            // ----------------------------------------------------------------------
            var excelTransferManager = new ExcelToSqlTransferManager(Configuration, new ExcelProducer(mappingStore), _consumerMock.Object);
            var sdmxFileTransferManager = new SdmxFileToSqlTransferManager(Configuration, new SdmxFileProducer(mappingStore), _consumerMock.Object);
            var transferMngrUrl = new UrlToSqlTransferManager(Configuration, new UrlProducer(mappingStore), _consumerMock.Object);
            // ----------------------------------------------------------------------

            _controller = new ImportController(
                _dbManagerMock.Object,
                _contextAccessorMock.Object,
                _authManagementMock.Object,
                excelTransferManager,
                sdmxFileTransferManager,
                transferMngrUrl,
                Configuration,
                _authConfig,
                _mailServiceMock.Object,
                _backgroundQueueMock.Object
            );
        }

        [TearDown]
        public void TearDown()
        {
            _authManagementMock.Invocations.Clear();
            _backgroundQueueMock.Invocations.Clear();
            _consumerMock.Invocations.Clear();
            _obsCount = 0;
        }

        [Test]
        public void ImportExcel()
        {
            var lang = "en";
            IFormFile eddFile = GetFile("eddFile", "edd/264D_264_SALDI-no-attributes.xml");
            IFormFile excelFile = GetFile("excelFile", "excel/264D_264_SALDI.xlsx");

            var result = _controller.ImportExcel(
                _dataSpace,
                lang,
                eddFile,
                excelFile,
                null,
                null,
                false
            );

            ImportAssert<ExcelToSqlTransferParam>(result, 100);
        }

        [Test]
        public void ImportFromPostedSdmxFile()
        {
            var lang = "en";
            var postedFile = GetFile("file", "data/264D_264_SALDI.csv");

            var result = _controller.ImportSdmxFile(
                _dataSpace,
                null,
                lang,
                postedFile,
                null,
                null,
                null,
                false
            );

            ImportAssert<SdmxFileToSqlTransferParam>(result, 101);
        }

        [Test]
        public void ImportFromSharedSdmxFile()
        {
            var result = _controller.ImportSdmxFile(
                _dataSpace,
                null,
                "en",
                null,
                "data/264D_264_SALDI.csv",
                null,
                null,
                false
            );

            ImportAssert<SdmxFileToSqlTransferParam>(result, 101);
        }

        [Test]
        public void ImportFromUrl()
        {
            // Allow file Scheme for a test
            DotStatServices.Transfer.Extensions
                .AllowedImportSchemes
                .Add(Uri.UriSchemeFile);

            var uri = new Uri(new FileInfo("data/264D_264_SALDI_compact.xml").FullName);

            var result = _controller.ImportSdmxFile(
                _dataSpace,
                null,
                "en",
                null,
                uri.AbsoluteUri,
                null,
                null,
                false
            );

            ImportAssert<UrlToSqlTransferParam>(result, 12);
        }

        private void ImportAssert<T>(ActionResult<OperationResult> result, int expectedReadObsCount) where T:TransferParam
        {
            // Authorization method is called
            _authManagementMock.Verify(mock => mock.IsAuthorized(
                    It.IsAny<DotStatPrincipal>(),
                    _dataSpace,
                    _dataflow.AgencyId,
                    _dataflow.Code,
                    _dataflow.Version.ToString(),
                    PermissionType.CanImportData
                ),
                Times.Once
            );

            // Background task is executed
            _backgroundQueueMock.Verify(mock => mock.Enqueue(
                    It.IsAny<Func<CancellationToken, Task>>()
                ),
                Times.Once
            );

            // Consumer.Save method is called
            _consumerMock.Verify(mock => mock.Save(
                    It.IsAny<T>(),
                    It.IsAny<Dataflow>(),
                    It.IsAny<TransferContent>()
                ),
                Times.Once
            );

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Value);
            Assert.IsInstanceOf<OperationResult>(result.Value);
            Assert.IsTrue(result.Value.Success);
            Assert.AreEqual(expectedReadObsCount, _obsCount);
        }

        private IFormFile GetFile(string name, string filename)
        {
            var fi = new FileInfo(filename);

            return new FormFile(fi.OpenRead(), 0, fi.Length, name, fi.Name);
        }
    }
}

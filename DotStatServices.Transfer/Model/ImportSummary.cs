﻿using System;
using System.Collections.Generic;
using DotStat.Domain;

namespace DotStatServices.Transfer.Model
{
    public sealed class ImportSummary
    {
        public string Dataspace { get; set; }
        public int RequestId { get; set; }
        public string UserEmail { get; set; }
        public string Artefact { get; set; }
        public DateTime SubmissionTime { get; set; }
        public DateTime ExecutionStart { get; set; }
        public DateTime? ExecutionEnd { get; set; }
        public string ExecutionStatus { get; set; }
        public string Outcome { get; set; } 
        public IEnumerable<TransactionLog> Logs { get; set; }
    }
}

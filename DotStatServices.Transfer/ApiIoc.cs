﻿using DotStat.Db.Manager;
using DotStat.Db.Manager.SqlServer;
using DotStat.Db.Repository;
using DotStat.Db.Repository.SqlServer;
using DotStat.MappingStore;
using DotStat.Transfer.Consumer;
using DotStat.Transfer.Manager;
using DotStat.Transfer.Param;
using DotStat.Transfer.Producer;
using DryIoc;
using System.Diagnostics.CodeAnalysis;
using DotStat.Common.Auth;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Localization;
using DotStatServices.Transfer.Services;
using Estat.Sri.MappingStoreRetrieval.Config;
using Estat.Sri.MappingStoreRetrieval.Factory;
using Estat.Sri.MappingStoreRetrieval.Helper;
using Microsoft.Extensions.Configuration;
using DotStat.Transfer.Processor;
using DotStat.Common.Logger;

namespace DotStatServices.Transfer
{
    [ExcludeFromCodeCoverage]
    internal class ApiIoc
    {
        public ApiIoc(IContainer container, IConfiguration configuration)
        {
            RegisterServices(container, configuration);
        }

        private static void RegisterServices(IContainer container, IConfiguration configuration)
        {
            var authConfig = configuration.GetSection("auth").Get<AuthConfiguration>() ?? AuthConfiguration.Default;
            var baseConfig = configuration.Get<BaseConfiguration>();

            container.UseInstance(baseConfig);
            container.UseInstance<IDataspaceConfiguration>(baseConfig);
            container.UseInstance<ILocalizationConfiguration>(baseConfig);
            container.UseInstance<IMailConfiguration>(baseConfig);
            container.UseInstance<IGeneralConfiguration>(baseConfig);
            container.UseInstance<IAuthConfiguration>(authConfig);

            LocalizationRepository.Configure(baseConfig);

            // NSI configuration ----------------------

            ConfigManager.Config.DataflowConfiguration.IgnoreProductionForStructure = true;
            ConfigManager.Config.DataflowConfiguration.IgnoreProductionForData = true;

            // DI registration ------------------------

            // Log4Net
            LogHelper.ConfigureAppenders(baseConfig);
            
            MappingStoreIoc.Container.Register<IRetrievalEngineContainerFactory, RetrievalEngineContainerFactory>(Reuse.Singleton);
            container.Register<IObservationRepository, SqlObservationRepository>(Reuse.Singleton);
            container.Register<IAttributeRepository, SqlAttributeRepository>(Reuse.Singleton);

            container.Register<ITransferProcessor, NoneTransferProcessor>(Reuse.Singleton);

            container.Register<ITransferManager<ExcelToSqlTransferParam>, ExcelToSqlTransferManager>(Reuse.Transient);
            container.Register<ITransferManager<SqlToSqlTransferParam>, SqlToSqlTransferManager>(Reuse.Transient);
            container.Register<ITransferManager<SdmxFileToSqlTransferParam>, SdmxFileToSqlTransferManager>(Reuse.Transient);
            container.Register<ITransferManager<UrlToSqlTransferParam>, UrlToSqlTransferManager>(Reuse.Transient);
            container.Register<CommonManager>(Reuse.Transient);

            container.Register<IProducer<ExcelToSqlTransferParam>, ExcelProducer>(Reuse.Transient);
            container.Register<IProducer<SqlToSqlTransferParam>, SqlProducer<SqlToSqlTransferParam>>(Reuse.Transient);
            container.Register<IProducer<SdmxFileToSqlTransferParam>, SdmxFileProducer>(Reuse.Transient);
            container.Register<IProducer<UrlToSqlTransferParam>, UrlProducer>(Reuse.Transient);

            container.Register<IConsumer<ExcelToSqlTransferParam>, SqlConsumer>(
                Reuse.Transient,
                setup: Setup.With(condition: r => r.Parent.ImplementationType == typeof(ExcelToSqlTransferManager)));

            container.Register<IConsumer<SqlToSqlTransferParam>, SqlConsumer>(
                Reuse.Transient,
                setup: Setup.With(condition: r => r.Parent.ImplementationType == typeof(SqlToSqlTransferManager)));

            container.Register<IConsumer<SdmxFileToSqlTransferParam>, SqlConsumer>(
                Reuse.Transient,
                setup: Setup.With(condition: r => r.Parent.ImplementationType == typeof(SdmxFileToSqlTransferManager)));

            container.Register<IConsumer<UrlToSqlTransferParam>, SqlConsumer>(
                Reuse.Transient,
                setup: Setup.With(condition: r => r.Parent.ImplementationType == typeof(UrlToSqlTransferManager)));

            container.Register<IDbManager, SqlServerDbManager>(Reuse.Transient);
            container.Register<IMappingStoreDataAccess, MappingStoreDataAccess>(Reuse.Scoped);
            container.Register<IMailService, MailService>(Reuse.Singleton);

            if (authConfig.Enabled)
            {
                container.Register<IAuthorizationRepository, SqlAuthorizationRepository>(Reuse.Singleton);
                container.Register<IAuthorizationManagement, AuthorizationManagement>(Reuse.Singleton);
            }
            else
            {
                container.Register<IAuthorizationManagement, NoAuthorizationManagement>(Reuse.Singleton);
            }
        }
    }
}
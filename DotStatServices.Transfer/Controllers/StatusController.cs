﻿using System;
using System.Collections.Generic;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Db.Manager;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using DotStat.Common.Auth;
using DotStat.Common.Logger;
using DotStat.Domain;
using DotStat.Transfer.Manager;
using DotStatServices.Transfer.Model;

namespace DotStatServices.Transfer.Controllers
{
    /// <summary>
    /// Status controller
    /// </summary>
    [ApiVersion("1.2")]
    public class StatusController : ControllerBase
    {
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly CommonManager _commonManager;
        private readonly IAuthorizationManagement _authorizationManagement;
        private readonly IAuthConfiguration _authConfiguration;
        private readonly BaseConfiguration _configuration;
        private readonly IDbManager _dbManager;

        /// <summary>
        /// Constructor for this controller filled by DryIoc
        /// </summary>
        public StatusController(
            IHttpContextAccessor contextAccessor,
            CommonManager commonManager,
            IAuthorizationManagement authorizationManagement,
            IAuthConfiguration authConfiguration,
            BaseConfiguration configuration,
            IDbManager dbManager
        )
        {
            _contextAccessor = contextAccessor;
            _commonManager = commonManager;
            _authorizationManagement = authorizationManagement;
            _authConfiguration = authConfiguration;
            _configuration = configuration;
            _dbManager = dbManager;
        }


        /// <summary>
        /// Get transaction information by transaction id, for a given dataspace
        /// </summary>
        /// <param name="dataspace">Dataspace</param>
        /// <param name="id">Transaction id (received during the submission of my request)</param>
        /// <returns></returns>
        /// <response code="200">Information successfully retreived</response>
        /// <response code="400">Validation or Internal error</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">Not found</response>
        [HttpPost]
        [Route("{version:apiVersion}/status/request")]
        public ActionResult<ImportSummary> StatusById(
            [FromForm, Required] string dataspace,
            [FromForm, Required] int id
        )
        {
            try
            {
                var userPrincipal = new DotStatPrincipal(_contextAccessor.HttpContext.User, _authConfiguration.ClaimsMapping);

                //Not logged in user
                if (_authConfiguration.Enabled && userPrincipal?.UserId == null)
                    return new ForbidResult();

                var space = dataspace.GetSpaceInternal(_configuration, "en");
                var managementRepository = _dbManager.GetManagementRepository(space.Id);
                var transaction = managementRepository.GetTransactionById(id);

                if (transaction == null)
                    return new NotFoundResult();

                var logs = managementRepository.GetTransactionLogs(id);

                var status = Extensions.GetTransactionStatus(transaction, space.DataImportTimeOutInMinutes);
                var transactionLogs = logs as TransactionLog[] ?? logs.ToArray();
                var outcome = Extensions.GetTransactionOutcome(status, transactionLogs);
                var submissionTime = Extensions.GetSubmissionTime(transaction, transactionLogs);

                return new ImportSummary()
                {
                    Dataspace = dataspace,
                    RequestId = id,
                    SubmissionTime = submissionTime,
                    ExecutionStart = transaction.ExecutionStart,
                    ExecutionEnd = transaction.ExecutionEnd,
                    UserEmail = transaction.UserEmail,
                    Artefact = transaction.ArtefactFullId,
                    ExecutionStatus = status.ToString(),
                    Outcome = outcome,
                    Logs = transactionLogs
                };
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw;
            }
        }

        /// <summary>
        /// Query transactions and logs for a given dataspace
        /// </summary>
        /// <param name="dataspace">Dataspace</param>
        /// <param name="userEmail">User email e.g. 'email@domain.com', default (any)</param>
        /// <param name="artefact">AGENCYID:ARTEFACTID(VERSION), default (any)</param>
        /// <param name="submissionStart">Datetime when the import/transfer was requested, default (24Hrs before now)</param>
        /// <param name="submissionEnd">Datetime when the import/transfer finished, default (now)</param>
        /// <param name="executionStatus">In progress (1), Completed (2), Timed OutAborted (3), default (any)</param>
        /// <param name="executionOutcome">Success (0), Warning (1), Error (2), None (3), default (any)</param>
        /// <returns></returns>
        /// <response code="200">Information successfully retrieved</response>
        /// <response code="400">Validation or Internal error</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">Not found</response>
        [HttpPost]
        [Route("{version:apiVersion}/status/requests")]
        public ActionResult<List<ImportSummary>> QueryRequests(
            [FromForm, Required] string dataspace,
            [FromForm] string userEmail,
            [FromForm] string artefact,
            [FromForm] DateTime? submissionStart,
            [FromForm] DateTime? submissionEnd,
            [FromForm] ExecutionStatus? executionStatus,
            [FromForm] TransactionOutcome? executionOutcome

        )
        {
            try
            {
                var userPrincipal = new DotStatPrincipal(_contextAccessor.HttpContext.User, _authConfiguration.ClaimsMapping);

                //Not logged in user
                if (_authConfiguration.Enabled && userPrincipal?.UserId == null)
                    return new ForbidResult();

                var language = string.IsNullOrEmpty(_configuration.DefaultLanguageCode)
                ? "en" : _configuration.DefaultLanguageCode;

                var space = dataspace.GetSpaceInternal(_configuration, language);
                var managementRepository = _dbManager.GetManagementRepository(space.Id);

                if (!string.IsNullOrEmpty(userEmail))
                    userEmail.IsValidEmail(true);

                var artefactFullId = "";
                if (!string.IsNullOrEmpty(artefact))
                    artefactFullId = artefact.GetArtefact(language).ToString();
                
                //Default 24 hrs before now
                submissionStart ??= DateTime.Now.AddDays(-1);
                //Default now
                submissionEnd ??= DateTime.Now;

                var transactions = managementRepository.GetTransactions(userEmail,
                    artefactFullId,
                    (DateTime)submissionStart,
                    (DateTime)submissionEnd,
                    null);
                //TODO: use status in the query once we store all the possible states of the transaction (submited, in progress, timedout, service interrupted, etc.) 

                if (transactions.Count==0)
                    return new NotFoundResult();

                var result = new List<ImportSummary>();
                foreach (var transaction in transactions)
                {
                    var logs = managementRepository.GetTransactionLogs(transaction.TransactionId);
                    var status = Extensions.GetTransactionStatus(transaction, space.DataImportTimeOutInMinutes);
                    var transactionLogs = logs as TransactionLog[] ?? logs.ToArray();
                    var outcome = Extensions.GetTransactionOutcome(status, transactionLogs);
                    var submissionTime = Extensions.GetSubmissionTime(transaction, transactionLogs);

                    //filtering over execution status
                    if (executionStatus != null && executionStatus.ToString() != status.ToString())
                        continue;

                    //filter by outcome
                    if (executionOutcome != null && outcome != executionOutcome.ToString()) continue;

                    result.Add(new ImportSummary()
                    {
                        Dataspace = dataspace,
                        RequestId = transaction.TransactionId,
                        //same as execution start, since we don't have a queuing system
                        SubmissionTime = submissionTime, 
                        ExecutionStart = transaction.ExecutionStart,
                        ExecutionEnd = transaction.ExecutionEnd,
                        Artefact = transaction.ArtefactFullId,
                        UserEmail = transaction.UserEmail,
                        ExecutionStatus = status.ToString(), //transactionExecutionStatus
                        Outcome = outcome,
                        Logs = transactionLogs
                    });
                    
                }
                if(result.Count==0)
                    return new NotFoundResult();

                return result;
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw;
            }
        }
    }
}

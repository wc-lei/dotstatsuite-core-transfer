﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Threading.Tasks;
using DotStat.Common.Auth;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Dto;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Db.Manager;
using DotStat.Domain;
using DotStat.Transfer.Manager;
using DotStat.Transfer.Param;
using DotStatServices.Transfer.BackgroundJob;
using DotStatServices.Transfer.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using static DotStat.Common.Localization.LocalizationRepository;

namespace DotStatServices.Transfer.Controllers
{
    /// <summary>
    /// Transfer controller
    /// </summary>
    [ApiVersion("1.2")]
    public class TransferController : ControllerBase
    {
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly IDbManager _dbManager;
        private readonly ITransferManager<SqlToSqlTransferParam> _transferMngrSql;
        private readonly ITransferManager<UrlToSqlTransferParam> _transferMngrUrl;
        private readonly BaseConfiguration _configuration;
        private IAuthConfiguration _authConfiguration;
        private readonly IMailService _mailService;
        private readonly BackgroundQueue _backgroundQueue;

        /// <summary>
        /// Constructor for this Controlled filled by DryIoc
        /// </summary>
        public TransferController(
            IHttpContextAccessor contextAccessor,
            IDbManager dbManager,
            ITransferManager<SqlToSqlTransferParam> transferMngrSql,
            ITransferManager<UrlToSqlTransferParam> transferMngrUrl,
            BaseConfiguration configuration,
            IAuthConfiguration authConfiguration,
            IMailService mailService,
            BackgroundQueue backgroundQueue
        )
        {
            _contextAccessor = contextAccessor;
            _dbManager = dbManager;
            _transferMngrSql = transferMngrSql;
            _transferMngrUrl = transferMngrUrl;
            _configuration = configuration;
            _authConfiguration = authConfiguration;
            _mailService = mailService;
            _backgroundQueue = backgroundQueue;
        }

        /// <summary>
        /// Initiates transfer of data between provided data spaces
        /// </summary>
        /// <param name="sourceDataspace">Source dataspace</param>
        /// <param name="sourceDataflow">Source AGENCYID:DATAFLOWID(VERSION)</param>
        /// <param name="sourceQuery">Source query (e.g: D.NOK.EUR.SP00.A)</param>
        /// <param name="destinationDataspace">Destination dataspace</param>
        /// <param name="destinationDataflow">Destination AGENCYID:DATAFLOWID(VERSION)</param>
        /// <param name="targetVersion">Target table version LIVE or PIT</param>
        /// <param name="PITReleaseDate">Point in time release date</param>
        /// <param name="restorationOptionRequired">Keep current LIVE version for restoration after PIT release</param>
        /// <param name="lang">Language code</param>
        /// <returns></returns>
        /// <response code="200">Transfer task successfully submitted</response>
        /// <response code="400">Validation or Internal error</response>    
        [HttpPost]
        [Route("{version:apiVersion}/transfer/dataflow")]
        public ActionResult<OperationResult> TransferDataflow(
            [FromForm, Required] string sourceDataspace,
            [FromForm, Required] string sourceDataflow,
            [FromForm] string sourceQuery,
            [FromForm, Required] string destinationDataspace,
            [FromForm, Required] string destinationDataflow,
            [FromForm] string lang,
            [FromForm] TargetVersion? targetVersion,
            [FromForm] string PITReleaseDate,
            [FromForm] Boolean restorationOptionRequired
            )
        {

            //set LIVE version as default
            if (targetVersion == null)
                targetVersion = TargetVersion.Live;

            if (targetVersion != TargetVersion.PointInTime)
            {
                PITReleaseDate = null;
                restorationOptionRequired = false;
            }
            
            var language = lang ?? _configuration.DefaultLanguageCode;

            try
            {
                var sourceSpace = sourceDataspace.GetSpaceInternal(_configuration, language);
                var destinationSpace = destinationDataspace.GetSpaceInternal(_configuration, language);

                var sqlManagementRepository = _dbManager.GetManagementRepository(destinationSpace.Id);
                var transactionId = sqlManagementRepository.GetNextTransactionId();
                
                var transferParam = new SqlToSqlTransferParam()
                {
                    Id = transactionId,
                    SourceDataspace = sourceSpace,
                    SourceDataflow = sourceDataflow.GetDataflow(language),
                    DestinationDataspace = destinationSpace,
                    DestinationDataflow = destinationDataflow.GetDataflow(language),
                    SourceQuery = sourceQuery,
                    CultureInfo = CultureInfo.GetCultureInfo(lang ?? _configuration.DefaultLanguageCode),
                    TargetVersion = (TargetVersion) targetVersion,
                    PITReleaseDate = PITReleaseDate.GetPITReleaseDate(language),
                    PITRestorationAllowed = restorationOptionRequired
                };
                    
                var requestMsg = DoTransfer(_transferMngrSql, transferParam);

                return new OperationResult(true, requestMsg);
            }
            catch (Exception exception)
            {
                Log.Error(exception);
                throw;
            }
        }

        private string DoTransfer<T>(ITransferManager<T> transferManager, T transferParam) where T : TransferParam, ISqlTransferParam
        {
            transferParam.Principal = new DotStatPrincipal(_contextAccessor.HttpContext.User, _authConfiguration.ClaimsMapping);

            LogHelper.RecordNewTransaction(transferParam.Id, transferParam.DestinationDataspace);
            Log.SetTransactionId(transferParam.Id);
            Log.SetDataspaceId(transferParam.DestinationDataspace.Id);
            
            var message = string.Format(
                GetLocalisedResource(
                    Localization.ResourceId.SubmissionResult,
                    transferParam.CultureInfo.TwoLetterISOLanguageName),
                transferParam.Id);

            Log.Notice(message);

            //Set data source information for email message
            var dataSource = string.IsNullOrEmpty(transferParam.SourceQuery) ?
                GetLocalisedResource(
                    Localization.ResourceId.EmailSummaryDataSourceTransactionNoQuery,
                    transferParam.CultureInfo.TwoLetterISOLanguageName)
                : string.Format(
                format: GetLocalisedResource(
                    Localization.ResourceId.EmailSummaryDataSourceTransactionQuery,
                    transferParam.CultureInfo.TwoLetterISOLanguageName), arg0: transferParam.SourceQuery);

            _backgroundQueue.Enqueue(async cancellationToken =>
            {
                Log.SetTransactionId(transferParam.Id);
                Log.SetDataspaceId(transferParam.DestinationDataspace.Id);

                var transactionResult = new TransactionResult()
                {
                    TransactionType = TransactionType.Transfer,
                    TransactionId = transferParam.Id,
                    DataSource = dataSource,
                    SourceDataSpaceId = transferParam.SourceDataspace?.Id,
                    DestinationDataSpaceId = transferParam.DestinationDataspace?.Id,
                    Dataflow = transferParam.DestinationDataflow.FullId(),
                    User = transferParam.Principal.Email,
                    TransactionStatus = TransactionStatus.TimedOutAborted
                };

                try
                {
                    transferManager.Transfer(transferParam);
                    transactionResult.TransactionStatus = TransactionStatus.Completed;
                }
                catch (Exception exception)
                {
                    Log.Warn(GetLocalisedResource(
                        Localization.ResourceId.NoObservationsProcessed,
                        transferParam.CultureInfo.TwoLetterISOLanguageName)
                    );
                    Log.Error(exception);
                }
                finally
                {
                    _mailService.SendMail(
                        transactionResult,
                        transferParam.CultureInfo.TwoLetterISOLanguageName
                    );
                }

                await Task.CompletedTask;
            });


            return message;
        }


    }
}
﻿using DotStat.Common.Auth;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Db.Manager;
using DotStat.Domain;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using DotStat.Db.Dto;
using DotStat.Transfer.Exception;
using DotStat.Transfer.Manager;
using DotStat.Transfer.Param;

namespace DotStatServices.Transfer.Controllers
{
    /// <summary>
    /// CleanUp controller
    /// </summary>
    [ApiVersion("1.2")]
    public class CleanUpController : ControllerBase
    {
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly IAuthConfiguration _authConfiguration;
        private readonly BaseConfiguration _configuration;
        private readonly IDbManager _dbManager;
        private readonly CommonManager _commonManager;

        /// <summary>
        /// Constructor for this controller filled by DryIoc
        /// </summary>
        public CleanUpController(
            IHttpContextAccessor contextAccessor,
            IAuthConfiguration authConfiguration,
            BaseConfiguration configuration,
            IDbManager dbManager,
            CommonManager commonManager)
        {
            _contextAccessor = contextAccessor;
            _authConfiguration = authConfiguration;
            _configuration = configuration;
            _dbManager = dbManager;
            _commonManager = commonManager;
        }

        /// <summary>
        /// Delete orphan DSDs from data db for provided dataspace
        /// </summary>
        /// <param name="dataspace"></param>
        /// <response code="200">Cleanup succeeded</response>
        /// <response code="400">Validation or Internal error</response>
        /// <response code="409">Conflict. There is an ongoing transaction for a DSD</response>
        /// <response code="207">Multi-Status, containing http status codes 200 and 409</response> 
        [HttpDelete]
        [Route("{version:apiVersion}/cleanup/orphans")]
        public ActionResult<OperationResult> CleanUpOrphanDsds(
        [FromForm, Required] string dataspace
        )
        {
            var transferParam = new TransferParam()
            {
                CultureInfo = CultureInfo.GetCultureInfo(_configuration.DefaultLanguageCode),
                DestinationDataspace = dataspace.GetSpaceInternal(_configuration, _configuration.DefaultLanguageCode),
                Principal = new DotStatPrincipal(_contextAccessor.HttpContext.User, _authConfiguration.ClaimsMapping),
                TargetVersion = TargetVersion.Live,
                PITReleaseDate = null
            };

            var sqlTransactionRepository = _dbManager.GetTransactionRepository(transferParam.DestinationDataspace.Id);
            var sqlManagementRepository = _dbManager.GetManagementRepository(transferParam.DestinationDataspace.Id);
            
            LogHelper.RecordNewTransaction(transferParam.Id, transferParam.DestinationDataspace);
            Log.SetTransactionId(transferParam.Id);
            Log.SetDataspaceId(transferParam.DestinationDataspace.Id);

            IList<ArtefactItem> cleanedDsds = new List<ArtefactItem>();
            IList<ArtefactItem> failedCleanedDsds = new List<ArtefactItem>();

            try
            {
                var dsdsInMgmntDb = sqlManagementRepository.GetListOfArtefacts("DSD");

                foreach (var dsdItm in dsdsInMgmntDb)
                {
                    transferParam.Id = sqlManagementRepository.GetNextTransactionId();
                    var dataStructure = dsdItm.ToString().GetDsd(_configuration.DefaultLanguageCode);

                    try
                    {
                        _commonManager.CleanUpDsd(transferParam, dataStructure, sqlManagementRepository,
                            sqlTransactionRepository);

                        cleanedDsds.Add(dsdItm);
                    }
                    catch (DatastructureStillExistsException)
                    {
                        //Dsd exists in structure db, no cleaning needed
                    }
                    catch (ConcurrentTransactionException)
                    {
                        failedCleanedDsds.Add(dsdItm);
                    }
                }

                if (failedCleanedDsds.Any() && !cleanedDsds.Any()) //http status code 409 Conflict
                {
                    return new ConflictObjectResult(new OperationResult(false,
                        string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                                .CleanUpErrorConcurrentTransaction),
                            string.Join(", ", failedCleanedDsds.Select(s => s.ToString())))));
                }

                if (failedCleanedDsds.Any() && cleanedDsds.Any()) //http status code 207 Multi-Status: 409 and 200  
                {
                    return StatusCode(StatusCodes.Status207MultiStatus,  new ObjectResult[]
                        {
                            //operation result for http status code 200
                            new OkObjectResult(new OperationResult(true, string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CleanUpSuccessful),
                                string.Join(", ", cleanedDsds.Select(s => s.ToString()))))),
                            //operation result for http status code 409
                            new ConflictObjectResult(new OperationResult(false,string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CleanUpErrorConcurrentTransaction),
                                string.Join(", ", failedCleanedDsds.Select(s => s.ToString())))))
                        });
                }

                if (!cleanedDsds.Any()) //http status code 200, no orphans to clean
                {
                    return new OperationResult(true,
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NoOrphanDsdsToClean));
                }

                //200 orphans deleted
                return new OperationResult(true,
                    string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CleanUpSuccessful),
                        string.Join(", ", cleanedDsds.Select(s => s.ToString()))));
            }
            catch (TransferUnauthorizedException)
            {
                return new ForbidResult();
            }
            catch (Exception exception)
            {
                Log.Error(exception);
                throw;
            }
        }

        /// <summary>
        /// Deletes everything from the data db for the provided dsd
        /// </summary>
        /// <param name="dataspace">Dataspace</param>
        /// <param name="dsd">AGENCYID:DSDID(VERSION)</param>
        /// <response code="200">Cleanup succeeded</response>
        /// <response code="400">Validation or Internal error</response>
        /// <response code="409">Conflict. There is an ongoing transaction targeting the DSD</response>////// 
        [HttpDelete]
        [Route("{version:apiVersion}/cleanup/dsd")]
        public ActionResult<OperationResult> CleanUpDsd(
            [FromForm, Required] string dataspace,
            [FromForm, Required] string dsd)
        {
            var transferParam = new TransferParam()
            {
                CultureInfo = CultureInfo.GetCultureInfo(_configuration.DefaultLanguageCode),
                DestinationDataspace = dataspace.GetSpaceInternal(_configuration, _configuration.DefaultLanguageCode),
                Principal = new DotStatPrincipal(_contextAccessor.HttpContext.User, _authConfiguration.ClaimsMapping),
                TargetVersion = TargetVersion.Live,
                PITReleaseDate = null
            };

            var sqlTransactionRepository = _dbManager.GetTransactionRepository(transferParam.DestinationDataspace.Id);
            var sqlManagementRepository = _dbManager.GetManagementRepository(transferParam.DestinationDataspace.Id);

            transferParam.Id = sqlManagementRepository.GetNextTransactionId();

            LogHelper.RecordNewTransaction(transferParam.Id, transferParam.DestinationDataspace);
            Log.SetTransactionId(transferParam.Id);
            Log.SetDataspaceId(transferParam.DestinationDataspace.Id);

            try
            {
                var dataStructure = dsd.GetDsd(_configuration.DefaultLanguageCode);

                if (!_commonManager.CleanUpDsd(transferParam, dataStructure, sqlManagementRepository,
                    sqlTransactionRepository))
                {                 
                    // Returns false when there was nothing to delete
                    return new OperationResult(false,
                        string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                                .CleanUpErrorNothingToDelete), dsd));
                }

                // Cleanup is successful.
                return new OperationResult(true,
                    string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CleanUpSuccessful),
                        dsd));
            }
            catch (TransferUnauthorizedException)
            {
                return new ForbidResult();
            }
            catch (DatastructureStillExistsException)
            {
                return new OperationResult(false, string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CleanUpErrorDsdStillExists), dsd));
            }
            catch (ConcurrentTransactionException)
            {
                return new ConflictObjectResult(new OperationResult(false,
                    string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                            .CleanUpErrorConcurrentTransaction), dsd)));
            }
            catch (Exception exception)
            {
                Log.Error(exception);
                throw;
            }
        }
    }
}

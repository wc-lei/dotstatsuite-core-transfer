﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Threading.Tasks;
using DotStat.Common.Auth;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Db.Manager;
using DotStat.Transfer.Manager;
using DotStat.Transfer.Param;
using DotStatServices.Transfer.BackgroundJob;
using DotStatServices.Transfer.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DotStatServices.Transfer.Controllers
{
    /// <summary>
    /// Point In Time controller
    /// </summary>
    [ApiVersion("1.2")]
    public class PointInTimeController : ControllerBase
    {
        private readonly IDbManager _dbManager;
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly CommonManager _commonManager;
        private readonly BaseConfiguration _configuration;
        private readonly IAuthConfiguration _authConfiguration;
        private readonly IMailService _mailService;
        private readonly BackgroundQueue _backgroundQueue;

        /// <summary>
        /// Constructor for this Controlled filled by DryIoc
        /// </summary>
        public PointInTimeController(
            IDbManager dbManager,
            IHttpContextAccessor contextAccessor,
            CommonManager commonManager, 
            BaseConfiguration configuration,
            IAuthConfiguration authConfiguration,
            IMailService mailService,
            BackgroundQueue backgroundQueue
        )
        {
            _dbManager = dbManager;
            _contextAccessor = contextAccessor;
            _commonManager = commonManager;
            _configuration = configuration;
            _authConfiguration = authConfiguration;
            _mailService = mailService;
            _backgroundQueue = backgroundQueue;
        }

        /// <summary>
        /// Information about Point-In-Time release
        /// </summary>
        /// <param name="dataspace">Dataspace</param>
        /// <param name="dataflow">AGENCYID:DATAFLOWID(VERSION)</param>
        /// <returns></returns>
        /// <response code="200">Information successfully retreived</response>
        /// <response code="400">Validation or Internal error</response>
        /// <response code="401">Unauthorized</response>
        [HttpPost]
        [Route("{version:apiVersion}/pointintime/PITInfo")]
        public ActionResult<OperationResult> PitInfo(
            [FromForm, Required] string dataspace,
            [FromForm, Required] string dataflow
        )
        {
            var transferParam = new TransferParam()
            {
                SourceDataspace = dataspace.GetSpaceInternal(_configuration, _configuration.DefaultLanguageCode),
                Principal = new DotStatPrincipal(_contextAccessor.HttpContext.User, _authConfiguration.ClaimsMapping)
            };

            Log.SetDataspaceId(transferParam.SourceDataspace.Id);

            var sourceDataFlow = dataflow.GetDataflow(_configuration.DefaultLanguageCode);
            var sqlTransactionRepository = _dbManager.GetTransactionRepository(transferParam.SourceDataspace.Id);
            var PITInfo = _commonManager.GetPITInfo(transferParam, sourceDataFlow, sqlTransactionRepository);

            return new OkObjectResult(PITInfo);
        }

        /// <summary>
        /// Rollback of Point-In-Time release
        /// </summary>
        /// <param name="dataspace">Dataspace</param>
        /// <param name="dataflow">AGENCYID:DATAFLOWID(VERSION)</param>
        /// <returns></returns>
        /// <response code="200">Rollback successfully submited</response>
        /// <response code="400">Validation or Internal error</response>
        /// <response code="401">Unauthorized</response>
        [HttpPost]
        [Route("{version:apiVersion}/pointintime/rollback")]
        public ActionResult<OperationResult> Rollback(
            [FromForm, Required] string dataspace,
            [FromForm, Required] string dataflow
        )
        {
            var transferParam = new TransferParam()
            {
                CultureInfo = CultureInfo.GetCultureInfo(_configuration.DefaultLanguageCode),
                SourceDataspace = dataspace.GetSpaceInternal(_configuration, _configuration.DefaultLanguageCode),
                Principal = new DotStatPrincipal(_contextAccessor.HttpContext.User, _authConfiguration.ClaimsMapping)
            };

            return Rollback(transferParam, dataflow);
        }

        /// <summary>
        /// Restoration of already released PIT version
        /// </summary>
        /// <param name="dataspace">Dataspace</param>
        /// <param name="dataflow">AGENCYID:DATAFLOWID(VERSION)</param>
        /// <returns></returns>
        /// <response code="200">Restoration successfully submited</response>
        /// <response code="400">Validation or Internal error</response>
        /// <response code="401">Unauthorized</response>
        [HttpPost]
        [Route("{version:apiVersion}/pointintime/restoration")]
        public ActionResult<OperationResult> Restore(
             [FromForm, Required] string dataspace,
             [FromForm, Required] string dataflow
         )
        {
            var transferParam = new TransferParam()
            {
                CultureInfo = CultureInfo.GetCultureInfo(_configuration.DefaultLanguageCode),
                SourceDataspace = dataspace.GetSpaceInternal(_configuration, _configuration.DefaultLanguageCode),
                Principal = new DotStatPrincipal(_contextAccessor.HttpContext.User, _authConfiguration.ClaimsMapping)
            };

            return Restore(transferParam, dataflow);
        }

        #region private methods
        private ActionResult<OperationResult> Rollback(TransferParam transferParam, string dataflow)
        {
            var sqlTransactionRepository = _dbManager.GetTransactionRepository(transferParam.SourceDataspace.Id);
            var sqlManagementRepository = _dbManager.GetManagementRepository(transferParam.SourceDataspace.Id);

            transferParam.Id = sqlManagementRepository.GetNextTransactionId();

            // -----------------------------------------------------

            LogHelper.RecordNewTransaction(transferParam.Id, transferParam.SourceDataspace);
            Log.SetTransactionId(transferParam.Id);
            Log.SetDataspaceId(transferParam.SourceDataspace.Id);

            var message = string.Format(
                LocalizationRepository.GetLocalisedResource(
                    Localization.ResourceId.SubmissionResult,
                    transferParam.CultureInfo.TwoLetterISOLanguageName
                ),
                transferParam.Id
            );

            Log.Notice(message);

            _backgroundQueue.Enqueue(async cancellationToken =>
            {
                Log.SetTransactionId(transferParam.Id);
                Log.SetDataspaceId(transferParam.SourceDataspace.Id);

                var transactionResult = new TransactionResult()
                {
                    TransactionType = TransactionType.PitRollback,
                    TransactionId = transferParam.Id,
                    DestinationDataSpaceId = transferParam.SourceDataspace.Id, // NB not a typo
                    Dataflow = dataflow,
                    User = transferParam.Principal.Email,
                    TransactionStatus = TransactionStatus.TimedOutAborted
                };

                try
                {
                    _commonManager.Rollback(
                        transferParam, 
                        dataflow.GetDataflow(transferParam.CultureInfo.TwoLetterISOLanguageName), 
                        sqlManagementRepository, 
                        sqlTransactionRepository
                    );

                    transactionResult.TransactionStatus = TransactionStatus.Completed;
                }
                catch (Exception exception)
                {
                    Log.Error(exception);
                }
                finally
                {
                    _mailService.SendMail(
                        transactionResult,
                        transferParam.CultureInfo.TwoLetterISOLanguageName
                    );
                }

                await Task.CompletedTask;
            });

            return new OkObjectResult(new OperationResult(true, message));
        }

        private ActionResult<OperationResult> Restore(TransferParam transferParam, string dataflow)
        {
            var sqlTransactionRepository = _dbManager.GetTransactionRepository(transferParam.SourceDataspace.Id);
            var sqlManagementRepository = _dbManager.GetManagementRepository(transferParam.SourceDataspace.Id);

            transferParam.Id = sqlManagementRepository.GetNextTransactionId();

            // ---------------------------------------------

            LogHelper.RecordNewTransaction(transferParam.Id, transferParam.SourceDataspace);
            Log.SetTransactionId(transferParam.Id);
            Log.SetDataspaceId(transferParam.SourceDataspace.Id);

            var message = string.Format(
                LocalizationRepository.GetLocalisedResource(
                    Localization.ResourceId.SubmissionResult,
                    transferParam.CultureInfo.TwoLetterISOLanguageName
                ),
                transferParam.Id
            );

            Log.Notice(message);

            _backgroundQueue.Enqueue(async cancellationToken =>
            {
                Log.SetTransactionId(transferParam.Id);
                Log.SetDataspaceId(transferParam.SourceDataspace.Id);

                var transactionResult = new TransactionResult()
                {
                    TransactionType = TransactionType.PitRestore,
                    TransactionId = transferParam.Id,
                    DestinationDataSpaceId = transferParam.SourceDataspace.Id, // NB not a typo
                    Dataflow = dataflow,
                    User = transferParam.Principal.Email,
                    TransactionStatus = TransactionStatus.TimedOutAborted
                };

                try
                {
                    _commonManager.Restore(
                        transferParam,
                        dataflow.GetDataflow(transferParam.CultureInfo.TwoLetterISOLanguageName),
                        sqlManagementRepository,
                        sqlTransactionRepository
                    );

                    transactionResult.TransactionStatus = TransactionStatus.Completed;
                }
                catch (Exception exception)
                {
                    Log.Error(exception);
                }
                finally
                {
                    _mailService.SendMail(
                        transactionResult,
                        transferParam.CultureInfo.TwoLetterISOLanguageName
                    );
                }

                await Task.CompletedTask;
            });

            return new OkObjectResult(new OperationResult(true, message));
        }
        #endregion
    }
}
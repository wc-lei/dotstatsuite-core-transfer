﻿using DotStat.Common.Auth;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Db.Manager;
using DotStat.Domain;
using DotStat.MappingStore;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using DotStat.Transfer.Exception;
using DotStat.Transfer.Manager;
using DotStat.Transfer.Param;
using ImTools;

namespace DotStatServices.Transfer.Controllers
{
    /// <summary>
    /// CleanUp controller
    /// </summary>
    [ApiVersion("1.2")]
    public class DataManagementController : ControllerBase
    {
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly IAuthConfiguration _authConfiguration;
        private readonly IAuthorizationManagement _authorizationManagement;
        private readonly IMappingStoreDataAccess _mappingStoreDataAccess;
        private readonly BaseConfiguration _configuration;
        private readonly IDbManager _dbManager;
        private readonly CommonManager _commonManager;

        /// <summary>
        /// Constructor for this controller filled by DryIoc
        /// </summary>
        public DataManagementController(
            IHttpContextAccessor contextAccessor,
            IAuthConfiguration authConfiguration,
            IAuthorizationManagement authorizationManagement,
            IMappingStoreDataAccess mappingStoreDataAccess, 
            BaseConfiguration configuration,
            IDbManager dbManager,
            CommonManager commonManager)
        {
            _contextAccessor = contextAccessor;
            _authConfiguration = authConfiguration;
            _authorizationManagement = authorizationManagement;
            _mappingStoreDataAccess = mappingStoreDataAccess;
            _configuration = configuration;
            _dbManager = dbManager;
            _commonManager = commonManager;
        }

        /// <summary>
        /// Initializes database objects of a dataflow in datastore database
        /// </summary>
        /// <param name="dataspace">Dataspace</param>
        /// <param name="dataflow">SDMX id of dataflow in the format of AGENCY_ID:DATAFLOW_ID(VERSION)</param>
        /// <response code="200">Initialization has already been done</response>
        /// <response code="201">Initialization successfully completed</response>
        /// <response code="400">Validation or Internal error</response>
        /// <response code="401">Not authorized.</response>
        /// <response code="409">Conflict. There is an ongoing transaction targeting the DSD of the dataflow</response>
        [Microsoft.AspNetCore.Mvc.HttpPost]
        [Microsoft.AspNetCore.Mvc.Route("{version:apiVersion}/init/dataflow")]
        public ActionResult<OperationResult> InitDatabaseObjectsOfDataflow(
            [FromForm, Required] string dataspace,
            [FromForm, Required] string dataflow)
        {
            var transferParam = new TransferParam()
            {
                CultureInfo = CultureInfo.GetCultureInfo(_configuration.DefaultLanguageCode),
                DestinationDataspace = dataspace.GetSpaceInternal(_configuration, _configuration.DefaultLanguageCode),
                Principal = new DotStatPrincipal(_contextAccessor.HttpContext.User, _authConfiguration.ClaimsMapping),
                TargetVersion = TargetVersion.Live,
                PITReleaseDate = null
            };

            var sqlTransactionRepository = _dbManager.GetTransactionRepository(transferParam.DestinationDataspace.Id);
            var sqlManagementRepository = _dbManager.GetManagementRepository(transferParam.DestinationDataspace.Id);

            transferParam.Id = sqlManagementRepository.GetNextTransactionId();

            LogHelper.RecordNewTransaction(transferParam.Id, transferParam.DestinationDataspace);
            Log.SetTransactionId(transferParam.Id);
            Log.SetDataspaceId(transferParam.DestinationDataspace.Id);

            try
            {
                if (!_commonManager.InitDataDbObjectsOfDataflow(transferParam, dataflow.GetDataflow(_configuration.DefaultLanguageCode), sqlManagementRepository,
                    sqlTransactionRepository))
                {
                    return new ConflictObjectResult(new OperationResult(false,
                        string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                                .InitDBObjectsOfDataflowConcurrentTransaction),
                            dataflow)));
                }

                return Created(String.Empty,
                    new OperationResult(true,
                        string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                                .InitDBObjectsOfDataflowSuccess), dataflow)));
            }
            catch (DataflowAlreadyInitializedException)
            {
                return Ok(new OperationResult(true,
                    string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                            .InitDBObjectsOfDataflowAlreadyDone), dataflow)));
            }
            catch (TransferUnauthorizedException)
            {
                return new ForbidResult();
            }
            catch (Exception exception)
            {
                Log.Error(exception);
                throw;
            }
        }
    }
}

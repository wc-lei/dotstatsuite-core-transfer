﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace DotStatServices.Transfer.HealthCheck
{
    /// <summary>
    /// 
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class DbHealthCheck : IHealthCheck
    {
        private readonly IDataspaceConfiguration _configuration;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        public DbHealthCheck(IDataspaceConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = new CancellationToken())
        {
            var result = true;
            var data = new Dictionary<string, object>();

            foreach (var space in _configuration.SpacesInternal)
            {
                var structureDbVersion = await CheckSqlServerConnection(space.DotStatSuiteCoreStructDbConnectionString, cancellationToken);
                var dataDbVersion = await CheckSqlServerConnection(space.DotStatSuiteCoreDataDbConnectionString, cancellationToken);

                data.Add(space.Id, new
                {
                    structureDbVersion,
                    dataDbVersion
                });

                result &= !string.IsNullOrEmpty(structureDbVersion) & !string.IsNullOrEmpty(dataDbVersion);
            }

            return result 
                ? HealthCheckResult.Healthy(data:data) 
                : HealthCheckResult.Unhealthy(data:data);
        }

        private async Task<string> CheckSqlServerConnection(string connectionString, CancellationToken cancellationToken)
        {
            try
            {
                await using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync(cancellationToken);

                    await using (var command = connection.CreateCommand())
                    {
                        command.CommandText = "select [Version] from [dbo].[DB_VERSION]";
                        
                        return await command.ExecuteScalarAsync(cancellationToken) as string;
                    }
                }
            }
            catch
            {
                return null;
            }
        }
    }
}

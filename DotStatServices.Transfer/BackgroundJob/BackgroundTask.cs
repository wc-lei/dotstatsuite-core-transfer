﻿using System;
using DotStat.Common.Logger;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace DotStatServices.Transfer.BackgroundJob
{
    internal static class BackgroundTask
    {
        // Background Hosted service for Import & Transfer tasks 
        // https://docs.microsoft.com/en-us/dotnet/standard/microservices-architecture/multi-container-microservice-net-applications/background-tasks-with-ihostedservice
        public static void Register(IServiceCollection services)
        {
            services.AddSingleton(new BackgroundQueue(OnError, 10, 500));
            services.AddSingleton<IHostedService, BackgroundQueueService>();
        }

        private static void OnError(Exception ex)
        {
            Log.Error(ex);
        }
    }
}

﻿using DotStat.Common.Configuration.Dto;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Localization;
using Microsoft.AspNetCore.Http;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using DotStat.Domain;
using log4net.Core;
using DotStat.Db.Dto;

namespace DotStatServices.Transfer
{
    internal static class Extensions
    {
        public static readonly HashSet<string> AllowedImportSchemes = new HashSet<string>()
        {
            Uri.UriSchemeHttp,
            Uri.UriSchemeHttps
        };

        public static DataspaceInternal GetSpaceInternal(this string space, IDataspaceConfiguration configuration, string lang, bool mandatory = true)
        {
            if (string.IsNullOrEmpty(space) && mandatory)
                throw new ArgumentException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DataSpaceNotProvided, lang));

            var dataspace = configuration
                                .SpacesInternal
                                .FirstOrDefault(x => x.Id.Equals(space, StringComparison.InvariantCultureIgnoreCase));

            if (dataspace == null && mandatory)
                throw new ArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DataSpaceNotFound, lang),
                    space));

            return dataspace;
        }
              

        public static string GetLocalPath(this IFormFile file, string name, string lang, params string[] extensions)
        {
            if(file==null || file.Length == 0)
                throw new ArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.FileAttachmentNotProvided,
                        lang), name));

            if (!extensions.Any(ext => file.FileName.EndsWith(ext, StringComparison.OrdinalIgnoreCase)))
                throw new ArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NotValidFileExtension, lang),
                    name, file.FileName, string.Join(",", extensions)));

            var path = Path.GetTempFileName();

            using (var stream = new FileStream(path, FileMode.Open)) 
                file.CopyTo(stream);

            return path;
        }

        public static bool IsValidContentType(this IFormFile file, string name, string lang, params string[] validTypes)
        {
            if (file == null || file.Length == 0)
                throw new ArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.FileAttachmentNotProvided,
                        lang), name));

            if (validTypes==null)
                throw new ArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NotValidContentType, lang),
                    file.FileName, string.Join(",", "")));

            var contentType = "application/octet-stream";
            try
            {
                if (!string.IsNullOrEmpty(file.ContentType))
                    contentType = file.ContentType;
            }
            catch
            {
                // ignored
            }

            if (!validTypes.Any(type => contentType.Equals(type,StringComparison.InvariantCultureIgnoreCase)))
                throw new ArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NotValidContentType, lang),
                    file.FileName, string.Join(",", validTypes)));
            
            return true;
        }

        public static DataflowMutableCore GetDataflow(this string dataflow, string lang)
        {
            if(string.IsNullOrEmpty(dataflow))
                throw new ArgumentException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DataFlowNotProvided, lang));

            var elements = dataflow.Split(new char[] { ':', '(', ')' }, StringSplitOptions.RemoveEmptyEntries);

            if (elements.Length != 3)
                throw new ArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.WrongDataFlowFormat, lang),
                    dataflow));

            return new DataflowMutableCore()
            {
                AgencyId = elements[0],
                Id = elements[1],
                Version = elements[2]
            };
        }

        public static string FullId(this IDataflowMutableObject df)
        {
            if(df == null)
                throw new ArgumentException(nameof(df));

            return $"{df.AgencyId}:{df.Id}({df.Version})";
        }

        public static ArtefactItem GetArtefact(this string artefact, string lang)
        {
            if (string.IsNullOrEmpty(artefact))
                throw new ArgumentException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DataFlowNotProvided, lang));

            var elements = artefact.Split(new char[] { ':', '(', ')' }, StringSplitOptions.RemoveEmptyEntries);

            if (elements.Length != 3)
                throw new ArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.WrongDataFlowFormat, lang),
                    artefact));

            return new ArtefactItem()
            {
                Agency = elements[0],
                Id = elements[1],
                Version = elements[2]
            };
        }

        public static DataStructureMutableCore GetDsd(this string dsd, string lang)
        {
            if (string.IsNullOrEmpty(dsd))
                throw new ArgumentException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DSDNotProvided, lang));

            var elements = dsd.Split(new char[] { ':', '(', ')' }, StringSplitOptions.RemoveEmptyEntries);

            if (elements.Length != 3)
                throw new ArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.WrongDSDFormat, lang),
                    dsd));

            return new DataStructureMutableCore()
            {
                AgencyId = elements[0],
                Id = elements[1],
                Version = elements[2]
            };
        }

        public static DateTime? GetPITReleaseDate(this string dateTime, string lang)
        {
            if (string.IsNullOrEmpty(dateTime))
                return null;
            const string format = "dd-MM-yyyy HH:mm:ss";
            try
            {
                return DateTime.ParseExact(dateTime, format, CultureInfo.InvariantCulture);
            }
            catch(FormatException)
            {
                throw new ArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.WrongPITReleaseDateFormat, lang),
                    dateTime, format));
            }
        }

        public static bool IsValidUrl(string url)
        {
            return Uri.TryCreate(url, UriKind.Absolute, out var uriResult) && AllowedImportSchemes.Contains(uriResult.Scheme);
        }

        public static bool IsAccessible(string externalSdmxSourceUrl)
        {
            var request = WebRequest.Create(externalSdmxSourceUrl);
            //Ideally a request to get only the headers (request.Method = WebRequestMethods.Http.Head;) should be made, but some nsiws might not support it
            //As a workaround the contentLength is set to 0, which will return only the headers.
            request.ContentLength = 0;

            try
            {
                using (var response = request.GetResponse() as HttpWebResponse)
                {
                    return response != null && response.StatusCode == HttpStatusCode.OK;
                }
            }
            catch (Exception)
            {
                throw new ArgumentException(message: string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.SdmxSourceHttpRequestException),
                    externalSdmxSourceUrl));
            }

        }

        public static TransactionStatus GetTransactionStatus(Transaction transaction, int transactionTimeOut)
        {
            return transaction.Successful switch
            {
                //Transaction in progress
                null when (DateTime.Now - transaction.ExecutionStart).TotalMinutes < transactionTimeOut =>
                    TransactionStatus.InProgress,
                //Transaction might have timedOut 
                null when (DateTime.Now - transaction.ExecutionStart).TotalMinutes >= transactionTimeOut =>
                    TransactionStatus.TimedOutAborted,
                //Transaction timedOut
                false when transaction.TimedOut == true ||
                    (transaction.ExecutionEnd - transaction.ExecutionStart).Value.TotalMinutes >= transactionTimeOut =>
                    TransactionStatus.TimedOutAborted,
                //Transaction failed
                false => TransactionStatus.Completed,
                //Completed 
                _ => TransactionStatus.Completed
            };
        }

        public static string GetTransactionOutcome(TransactionStatus status, TransactionLog[] transactionLogs)
        {
            if (status == TransactionStatus.Submitted || status == TransactionStatus.InProgress
                || !transactionLogs.Any())//not yet finish processing or no logs
                return TransactionOutcome.None.ToString();
            else if (status == TransactionStatus.TimedOutAborted)
                return TransactionOutcome.Error.ToString();
            else if (transactionLogs.Any(l => l.Level == Level.Error.ToString()))//at least one error
                return TransactionOutcome.Error.ToString();
            else if (transactionLogs.Any(l => l.Level == Level.Warn.ToString()))//at least one warning
                return TransactionOutcome.Warning.ToString();
            else //Processing finished with no errors no warnings
                return TransactionOutcome.Success.ToString();
        }

        internal static DateTime GetSubmissionTime(Transaction transaction, TransactionLog[] transactionLogs)
        {
            var submissionTime = transaction.ExecutionStart;
            var submissionTimeLoggedText = string.Format(
                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.SubmissionResult),
                transaction.TransactionId);

            var loggedSubmissionTime =  transactionLogs.Where(log => log.Message == submissionTimeLoggedText)
                    .Select(log => log.Date).FirstOrDefault();
            if (loggedSubmissionTime != null)
                    submissionTime = (DateTime)loggedSubmissionTime;
 
            return submissionTime;
        }

        public static bool IsValidEmail(this string email, bool throwError= false)
        {
            try
            {
                return new System.Net.Mail.MailAddress(email).Address == email;
            }
            catch
            {
                return throwError
                    ? throw new ArgumentException(
                        message: LocalizationRepository.GetLocalisedResource(
                            Localization.ResourceId.EmailParameterError))
                    : false;
            }
        }

    }
}

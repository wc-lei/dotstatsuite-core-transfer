namespace DotStatServices.Transfer.Services
{
    /// <summary>
    /// 
    /// </summary>
    public interface IMailService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataspace"></param>
        /// <param name="transactionId"></param>
        /// <param name="transactionLogs"></param>
        /// <param name="mailTo"></param>
        /// <param name="languageCode"></param>
        void SendMail(TransactionResult transferResult, string languageCode);
    }
}
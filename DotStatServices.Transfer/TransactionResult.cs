﻿using DotStat.Common.Logger;
using log4net.Core;
using System.Runtime.Serialization;

namespace DotStatServices.Transfer
{
    public enum TransactionType
    {
        Import,
        Transfer,
        PitRollback,
        PitRestore
    }
    public enum TransactionStatus
    {
        Submitted,
        InProgress,
        Completed, //success/fail, but the process execution has finished.
        [EnumMember(Value = "Timed Out/Aborted")]
        TimedOutAborted// The process execution did not finish due to service interruption, transaction timeout etc.
    }

    /// <summary>
    /// Execution status: InProgress (1), Completed (2), TimedOutAborted (3), default (any)
    /// </summary>
    public enum ExecutionStatus
    {
        InProgress = TransactionStatus.InProgress,
        Completed,
        TimedOutAborted = TransactionStatus.TimedOutAborted,
    }

    /// <summary>
    /// Outcome of the transaction: Success (0), Warning (1), Error (2), None (3), default (any)
    /// </summary>
    public enum TransactionOutcome
    {
        /// <summary>
        /// Transaction completed successfully
        /// </summary>
        Success,
        /// <summary>
        /// Transaction completed with warnings
        /// </summary>
        Warning,
        /// <summary>
        /// Transaction failed
        /// </summary>
        Error,
        /// <summary>
        /// No information
        /// </summary>
        None
    }

    public class TransactionResult
    {

        public TransactionType TransactionType { get; set; }
        public int TransactionId { get; set; }
        public string SourceDataSpaceId { get; set; }
        public string DestinationDataSpaceId { get; set; }
        public string Dataflow { get; set; }
        public string User { get; set; }
        public TransactionStatus TransactionStatus { get; set; }
        public string DataSource { get; set; }

        public LoggingEvent[] TransactionLogs => LogHelper.GetRecordedEvents(TransactionId, DestinationDataSpaceId);

    }
}
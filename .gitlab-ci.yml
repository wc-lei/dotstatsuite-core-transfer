stages:
    - test #mandatory stage for SAST  
    - publish
    - publish-tag
    - smoke-test
    - deploy-qa
    - deploy-staging
    - smoke-test-qa
    - smoke-test-staging
    - load-test-qa
    - load-test-staging

include:
    - template: SAST.gitlab-ci.yml
    
variables:
    IMAGE: siscc/dotstatsuite-core-transfer
    DOCKER_HOST: tcp://docker:2375/
    DOCKER_DRIVER: overlay2
    TRANSFER_HOSTNAME_QA: "http://transfer-qa-oecd.redpelicans.com"
    TRANSFER_HOSTNAME_STAGING: "http://transfer-siscc.redpelicans.com"

unit test:
    stage: test
    image: mcr.microsoft.com/dotnet/core/sdk:3.1
    script:
      - dotnet restore
      - dotnet build DotStat.Transfer
      - dotnet test DotStat.Transfer.Test -p:CollectCoverage=true -p:CoverletOutputFormat=opencover
    coverage: /Total\s*\|\s*([\d\.]+)/
    except:
      - tags  
  

publish:
    stage: publish
    image: docker:latest    
    services:
        - docker:dind
    before_script:
        - docker login -u "$CI_USER_NAME" -p "$CI_USER_PWD"    
    script:
        - docker build --pull --build-arg NUGET_FEED=$NUGET_PRIVATE_FEED -t $IMAGE:$CI_COMMIT_SHORT_SHA .
        - docker tag $IMAGE:$CI_COMMIT_SHORT_SHA $IMAGE:$CI_COMMIT_REF_NAME
        - docker tag $IMAGE:$CI_COMMIT_SHORT_SHA $IMAGE:latest          
        - docker push $IMAGE:$CI_COMMIT_SHORT_SHA
        - docker push $IMAGE:$CI_COMMIT_REF_NAME
        - docker push $IMAGE:latest
    only:
        - develop
        - master
            
publish-tag:
    stage: publish-tag
    image: docker:latest
    services:
        - docker:dind
    before_script:
        - docker login -u "$CI_USER_NAME" -p "$CI_USER_PWD"       
    script:
        - docker build --pull --build-arg NUGET_FEED=$NUGET_PRIVATE_FEED -t $IMAGE:$CI_COMMIT_TAG .
        - docker push $IMAGE:$CI_COMMIT_TAG
    only:
        - tags
        
 
smoke-test:
    stage: smoke-test
    image: 
        name: docker/compose:latest
        entrypoint: ["/bin/sh", "-c"]
    services:
        - docker:dind 
    before_script:
        - apk update && apk upgrade && apk add git
        - apk add --update curl && rm -rf /var/cache/apk/*
        - git clone -b master --single-branch https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-docker-compose.git docker-compose  
        - cd docker-compose/dotnet        
        - docker-compose -f docker-compose-single-dataendpoint-reset.yml up -d transfer-service
    script:
        - chmod u+x docker_smoke_test.sh
        - ./docker_smoke_test.sh http://docker:93/live 300
    only:
        - develop
        - master
    
deploy-qa:
    stage: deploy-qa
    image: google/cloud-sdk
    before_script:
        - echo ${KUBE_ACCOUNT_KEY} | base64 -di > key.json
    script:
        - gcloud auth activate-service-account --key-file=key.json
        - gcloud config set project oecd-228113
        - gcloud config set container/cluster oecd-core
        - gcloud config set compute/region europe-west1-b
        - gcloud container clusters get-credentials oecd-core --region europe-west1-b
        - kubectl get pods -n qa -o wide
        - kubectl set image deployment/transfer transfer=$IMAGE:$CI_COMMIT_SHORT_SHA -n qa
        - kubectl set env deployment/transfer GIT_HASH=$CI_COMMIT_SHA -n qa
    only:
        - develop
    environment:
        name: qa
    tags:
        - kube
        - oecd

deploy-staging:
    stage: deploy-staging
    image: google/cloud-sdk
    before_script:
        - echo ${KUBE_ACCOUNT_KEY} | base64 -di > key.json
    script:
        - gcloud auth activate-service-account --key-file=key.json
        - gcloud config set project oecd-228113
        - gcloud config set container/cluster oecd-core
        - gcloud config set compute/region europe-west1-b
        - gcloud container clusters get-credentials oecd-core --region europe-west1-b
        - kubectl get pods -n staging -o wide
        - kubectl set image deployment/transfer transfer=$IMAGE:$CI_COMMIT_SHORT_SHA -n staging
        - kubectl set env deployment/transfer GIT_HASH=$CI_COMMIT_SHA -n staging
    only:
        - master
    environment:
        name: staging
    tags:
        - kube
        - oecd
          
k6-smoke-test-qa:
  stage: smoke-test-qa
  image: docker:git
  services:
    - docker:stable-dind
  script:
    - docker run --rm -v "$(pwd)":/k6 -w /k6 -e TRANSFER_SERVICE_HOSTNAME=$TRANSFER_HOSTNAME_QA -e DATASPACE="qa:stable" -e USERNAME=$PERFORMANCE_TEST_USER -e PASSWORD=$PERFORMANCE_TEST_USER_PWD loadimpact/k6:0.27.0 run ./PerformanceTests/smoke-test.js --summary-export=smoke-test-results-qa.json
  artifacts:
    reports:
      load_performance: smoke-test-results-qa.json
  only:
    - develop
    
k6-smoke-test-staging:
  stage: smoke-test-staging
  image: docker:git
  services:
    - docker:stable-dind
  script:
    - docker run --rm -v "$(pwd)":/k6 -w /k6 -e TRANSFER_SERVICE_HOSTNAME=$TRANSFER_HOSTNAME_STAGING -e DATASPACE="staging:SIS-CC-stable" -e USERNAME=$PERFORMANCE_TEST_USER -e PASSWORD=$PERFORMANCE_TEST_USER_PWD loadimpact/k6:0.27.0 run ./PerformanceTests/smoke-test.js --summary-export=smoke-test-results-staging.json
  artifacts:
    reports:
      load_performance: smoke-test-results-staging.json
  only:
    - master
    
k6-load-test-qa:
  stage: load-test-qa
  image: docker:git
  services:
    - docker:stable-dind
  script:
    - docker run --rm -v "$(pwd)":/k6 -w /k6 -e TRANSFER_SERVICE_HOSTNAME=$TRANSFER_HOSTNAME_QA -e DATASPACE="qa:stable" -e USERNAME=$PERFORMANCE_TEST_USER -e PASSWORD=$PERFORMANCE_TEST_USER_PWD loadimpact/k6:0.27.0 run ./PerformanceTests/load-test.js --summary-export=load-test-results-qa.json
  artifacts:
    reports:
      load_performance: load-test-results-qa.json
  when: manual
  only:
    - develop
    
k6-load-test-staging:
  stage: load-test-staging
  image: docker:git
  services:
    - docker:stable-dind
  script:
    - docker run --rm -v "$(pwd)":/k6 -w /k6 -e TRANSFER_SERVICE_HOSTNAME=$TRANSFER_HOSTNAME_STAGING -e DATASPACE="staging:SIS-CC-stable" -e USERNAME=$PERFORMANCE_TEST_USER -e PASSWORD=$PERFORMANCE_TEST_USER_PWD loadimpact/k6:0.27.0 run ./PerformanceTests/load-test.js --summary-export=load-test-results-staging.json
  artifacts:
    reports:
      load_performance: load-test-results-staging.json
  when: manual
  only:
    - master
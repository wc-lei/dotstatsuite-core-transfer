# HISTORY

## 5.0.0 (MSDB v6.9, DataDB v2.1)
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/93
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/83
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/79
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/95
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/29
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/78
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/94
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-config/-/issues/1
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/96
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/101
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/106
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/111
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/46
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/117
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/100
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/49
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/107
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/101
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/102

## v4.2.4 2020-04-18 (MSDB v6.7, DataDB v2.1)
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/65


## v4.2.3 2020-04-18 (MSDB v6.7, DataDB v2.1)

- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/97
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/47
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/84
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/3
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/62
- https://gitlab.com/sis-cc/dotstatsuite-documentation/-/issues/65
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/58
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/65


## v4.1.2 2020-03-30 (MSDB v6.7, DataDB v2.1)

https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/-/issues/42
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/54
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/75
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/71
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/80
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/88
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/85
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/77
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/86
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/76
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/82
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/91
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/92


##  v4.0.3 2020-01-29 (MSDB v6.7, DataDB v2.1)
This release contains breaking changes with changes to the authentication management.

- /sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/66
- /sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/52


##  v3.0.1 2020-01-22 (MSDB v6.7, DataDB v2.1)
This release contains breaking changes with a new entry in the dataspaces.private.json and the introduction of localization.json via the Dotstat.config nuget package.     

- sis-cc/.stat-suite/dotstatsuite-core-common/issues/102
﻿using System;
using DotStat.Common.Localization;
using DotStat.Domain;
using DotStat.Transfer.Excel.Exceptions;
using DotStat.Transfer.Excel.Mapping;
using DotStat.Transfer.Excel.Reader;
using DotStat.Transfer.Excel.Util;
using Org.Sdmxsource.Sdmx.Api.Model.Data;

namespace DotStat.Transfer.Excel.Excel
{
    public sealed class ExcelCellDimensionAttrIterator : ExcelCellRangeIterator, IRecordIterator<IKeyable>
    {
        private string _name;
        private GroupAttrMapper _mapper;

        private new ExcelDimAttributesDescriptor CoordDescriptor { get;}

        public ExcelCellDimensionAttrIterator(ExcelDimAttributesDescriptor descriptor, Dataflow dataflow, IExcelDataSource dataSource, string worksheet) : base(descriptor, dataSource, worksheet, true)
        {
            CoordDescriptor = descriptor;
            _mapper = new GroupAttrMapper(dataflow);

            BuildFieldList();
            InitExpressions();
            Reset();
        }

        public override string Name => null;

        public override SWMapper Mapper => _mapper;
        V8Mapper<IKeyable> IRecordIterator<IKeyable>.Mapper => _mapper;

        public override string GetField(int pos)
        {
            if (pos < ExternCoordinates.Length)
                return ExternCoordinates[pos].Value;

            throw new InvalidOperationException(
                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelUnknownIndex));
        }

        protected override CellValue[] GetExternCoordinates()
        {
            var res = new CellValue[FieldCount];

            res[0].Value = CoordDescriptor.AttributeExpression.Result;
            res[1].Value = Current.Value;

            foreach (var dimExpr in CoordDescriptor.DimensionExpressions)
                res[dimExpr.Index].Value = dimExpr.Result;

            return res;
        }

        protected override void ResetCurrentDataExpressions(bool resetOwner = false)
        {
            base.ResetCurrentDataExpressions(resetOwner);

            if (resetOwner || CoordDescriptor.AttributeExpression.Owner != this)
            {
                CoordDescriptor.AttributeExpression.Owner = this;
            }
            else
            {
                CoordDescriptor.AttributeExpression.Reset();
            }

            foreach (var dc in CoordDescriptor.DimensionExpressions)
            {
                if (resetOwner || dc.Owner != this)
                {
                    dc.Owner = this;
                }
                else
                {
                    dc.Reset();
                }
            }
        }

        private void BuildFieldList()
        {
            for (var i = 0; i < _mapper.SourceNames.Count; i++)
            {
                _FieldsByName.Add(_mapper.SourceNames[i], i);
                _FieldsById.Add(i, _mapper.SourceNames[i]);
            }
        }

        private void InitExpressions()
        {
            CoordDescriptor.AttributeExpression.Init(0, this);

            foreach (var dimExpr in CoordDescriptor.DimensionExpressions)
            {
                int index;

                if (!_FieldsByName.TryGetValue(dimExpr.Code, out index))
                    throw new ApplicationArgumentException(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DimensionDoesNotBelongToDataset).F(dimExpr.Code, _mapper.TargetDataset.Code));

                dimExpr.Init(index, this);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace DotStat.Transfer.Excel.Excel
{
    public interface IExcelDataSource : IDisposable
    {
        string Name { get; }

        string GetCellValue(string cellAddress, string sheetName = null);
        string GetCellValue(int row, int column, string sheetName = null);
        string GetFormattedCellValue(int row, int column, string sheetName = null);
        string GetFormattedCellValue(string cellAddress, string sheetName = null);

        string GetCellComment(string cellAddress, string sheetName = null);
        string GetCellComment(int row, int column, string sheetName);

        void SetCellValue(object value, string cellAddress, string sheetName = null);
        void SetFormattedCellValue(object value, string cellAddress, string sheetName = null);

        void SetCellComment(string comment, string cellAddress, string sheetName = null);

        IEnumerable<string> Worksheets { get; }
        string CurrentWorksheet { get; set; }

        string WorkbookName { get; }
        int GetRowCount(string sheetName = null);
        int GetColumnCount(string sheetName = null);

        bool IsCellValueValid(int row, int column, string sheetName = null);

        /// <summary>
        /// Find columns with cells matching a given pattern
        /// </summary>
        /// <param name="sheetName"></param>
        /// <param name="range"></param>
        /// <param name="pattern">If pattern is null returns all the cells in the range</param>
        /// <param name="isRegex"></param>
        /// <param name="nonMatching">if true, returns cells that does not match the pattern, instead of matching ones</param>
        /// <returns></returns>
        IEnumerable<int> FindColumns(string sheetName, string range, string pattern, bool isRegex = false,
            bool nonMatching = false);

        /// <summary>
        /// Find rows with cells matching a given pattern
        /// </summary>
        /// <param name="sheetName"></param>
        /// <param name="range"></param>
        /// <param name="pattern">If pattern is null returns all the cells in the range</param>
        /// <param name="isRegex"></param>
        /// <param name="nonMatching">if true, returns cells that does not match the pattern, instead of matching ones</param>
        /// <returns></returns>
        IEnumerable<int> FindRows(string sheetName, string range, string pattern, bool isRegex = false,
            bool nonMatching = false);

        /// <summary>
        /// Find a cell matching a given pattern
        /// </summary>
        /// <param name="sheetName"></param>
        /// <param name="range"></param>
        /// <param name="pattern">If pattern is null returns all the cells in the range</param>
        /// <param name="isRegex"></param>
        /// <param name="nonMatching">if true, returns cells that does not match the pattern, instead of matching ones</param>
        /// <returns></returns>
        IEnumerable<Point> FindCells(string sheetName, string range, string pattern, bool isRegex = false,
            bool nonMatching = false);

        /// <summary>
        /// return the first cell matching
        /// </summary>
        /// <param name="sheetName"></param>
        /// <param name="range"></param>
        /// <param name="pattern">if the pattern is null, return the first cell in the range</param>
        /// <param name="isRegex"></param>
        /// <param name="nonMatching">if true, returns cells that does not match the pattern, instead of matching ones</param>
        /// <returns></returns>
        Point? FindFirstCell(string sheetName, string range, string pattern, bool isRegex = false,
            bool nonMatching = false);

        /// <summary>
        /// return the last cell matching
        /// </summary>
        /// <param name="sheetName"></param>
        /// <param name="range"></param>
        /// <param name="pattern">if the pattern is null, return the last cell in the range</param>
        /// <param name="isRegex"></param>
        /// <param name="nonMatching">if true, returns cells that does not match the pattern, instead of matching ones</param>
        /// <returns></returns>
        Point? FindLastCell(string sheetName, string range, string pattern, bool isRegex = false,
            bool nonMatching = false);

        int LastCellRow(string sheetName);
        int LastCellColumn(string sheetName);

        void Save();
    }
}
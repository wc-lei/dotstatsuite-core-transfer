﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Dynamic;
using DotStat.Common.Localization;
using DotStat.Domain;
using DotStat.Transfer.Excel.Exceptions;
using DotStat.Transfer.Excel.Util;
using OfficeOpenXml.FormulaParsing.Utilities;

namespace DotStat.Transfer.Excel.Excel
{
    public sealed class ConditionExpression : ExpresssionBase
    {
        public readonly string Condition;
        private Func<ConditionExpression, bool> _Func;
        private bool? _BoolValue;

        public ConditionExpression(string condition, string exprSource, string sheetName = null)
            : base(sheetName, exprSource)
        {
            Condition = condition;
        }

        private bool EvaluateExpression()
        {
            if (_Func == null)
            {
                _Func = BuildFunc<ConditionExpression, bool>(Condition);
            }
            bool res;
            try
            {
                res = _Func(this);
            }
            catch (UnrecoverableAppException ex)
            {
                throw new UnrecoverableAppException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.EddExpressionEvaluation),
                    Condition, ExpressionSource, ex.Message),
                    ex);
            }
            catch (System.Exception ex)
            {
                throw new ApplicationArgumentException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.EddExpressionEvaluation),
                        Condition, ExpressionSource, ex.Message),
                    ex);
            }

            return res;
        }

        public bool BooleanResult
        {
            get
            {
                if (!_BoolValue.HasValue)
                {
                    _BoolValue = EvaluateExpression();
                }

                return _BoolValue.Value;
            }
        }

        protected internal override void Reset()
        {
            base.Reset();
            _BoolValue = null;
        }
    }

    public class AxisReferenceExpression : ExpresssionBase
    {
        internal readonly string AxisExpression;
        internal readonly bool IsExpression;

        private Func<ExpresssionBase, object> _IndexFunc;
        private int? _Index;
        private int[] _IndexList;

        public AxisReferenceExpression(string axisExpression, string exprSource, bool isExpression = false)
            : base(null, exprSource)
        {
            if (string.IsNullOrWhiteSpace(axisExpression))
            {
                throw new ApplicationArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.EddAxisRefNotSpecified),
                    exprSource));
            }

            IsExpression = isExpression;
            AxisExpression = axisExpression;
            if (!isExpression)
            {
                EvaluationSource = axisExpression;
            }
            _Index = null;
        }

        protected internal override void Reset()
        {
            base.Reset();
            if (IsExpression)
            {
                _Index = null;
                _IndexList = null;
            }
        }

        public virtual string Result
        {
            get { return Value; }
        }

        private void EvaluateIndex()
        {
            if (_Index.HasValue)
            {
                return;
            }

            if (!IsExpression)
            {
                int val;

                if (!int.TryParse(AxisExpression, out val))
                {
                    throw new ApplicationArgumentException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.EddCannotParseAxis),
                        AxisExpression, Owner.Name, ExpressionSource));
                }

                _Index = val;
                _IndexList = new[] {_Index.Value};
                return;
            }

            if (_IndexFunc == null)
            {
                _IndexFunc = BuildFunc<ExpresssionBase, object>(AxisExpression);
            }

            object res;
            try
            {
                res = _IndexFunc(this);
            }
            catch (UnrecoverableAppException ex)
            {
                throw new UnrecoverableAppException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.EddExpressionEvaluation),
                        AxisExpression, ExpressionSource, ex.Message),
                    ex);
            }
            catch (System.Exception ex)
            {
                throw new ApplicationArgumentException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.EddExpressionEvaluation),
                        AxisExpression, ExpressionSource, ex.Message),
                    ex);
            }

            var rl = res as IEnumerable<int>;

            if (rl != null)
            {
                _IndexList = rl.ToArray();
                _Index = IndexList.Length > 0 ? IndexList[0] : -1;
            }
            else if (res != null)
            {
                try
                {
                    _Index = Convert.ToInt32(res);
                    _IndexList = new[] {_Index.Value};
                }
                catch (InvalidCastException ex)
                {
                    throw new ApplicationArgumentException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.EddIntegerExpression),
                            AxisExpression, ExpressionSource),
                        ex);
                }
            }
            else
            {
                _Index = null;
                _IndexList = new int[0];
            }
        }

        public int[] IndexList
        {
            get
            {
                if (_IndexList == null)
                {
                    EvaluateIndex();
                }

                return _IndexList;
            }
        }

        public int Index
        {
            get
            {
                if (!_Index.HasValue)
                {
                    EvaluateIndex();
                }

                // ReSharper disable PossibleInvalidOperationException
                return _Index.Value;
                // ReSharper restore PossibleInvalidOperationException
            }
        }
    }

    public class CellReferenceExpression : ExpresssionBase
    {
        public string CoordExpression { get; private set; }
        public readonly bool IsExpression;

        private Func<CellReferenceExpression, object> _ReferenceFunc;

        private IIndexer<CellReference> _Cells;
        private bool _ExprValueCalculated;
        private string _ExprValue;

        public CellReferenceExpression(string coordExpresssion, string exprSource, bool isExpression = false)
            : base(isExpression ? null : CellReference.ExtractSheetName(coordExpresssion), exprSource)
        {
            if (string.IsNullOrWhiteSpace(coordExpresssion))
            {
                throw new ApplicationArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.EddEmptyCellReference),
                    exprSource));
            }

            CoordExpression = coordExpresssion;
            IsExpression = isExpression;
            if (!isExpression)
            {
                EvaluationSource = coordExpresssion;
            }
        }

        protected virtual CellReference[] EvaluateCoordinates()
        {
            if (!IsExpression)
            {
                return new[]
                {
                    new CellReference(CoordExpression)
                    {
                        Owner = Owner
                    }
                };
            }

            if (_ReferenceFunc == null)
            {
                _ReferenceFunc = BuildFunc<CellReferenceExpression, object>(CoordExpression);
            }

            object res;
            try
            {
                res = _ReferenceFunc(this);
            }
            catch (InvalidCellReferenceException)
            {
                //an invalid cell reference exceptip
                res = new CellReference[0];
            }
            catch (UnrecoverableAppException ex)
            {
                throw new UnrecoverableAppException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.EddExpressionEvaluation),
                        CoordExpression, ExpressionSource, ex.Message),
                    ex);
            }
            catch (System.Exception ex)
            {
                throw new ApplicationArgumentException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.EddExpressionEvaluation),
                        CoordExpression, ExpressionSource, ex.Message),
                    ex);
            }

            var res2 = InterpretExpressionResult(Owner, res);
            EvaluationSource = string.Join(";", res2.Select(ex => ex.Coordinates));
            return res2;
        }

        internal IIndexer<CellReference> CellReferenceListResult
        {
            get
            {
                if (_Cells == null)
                {
                    var ec = EvaluateCoordinates();
                    _Cells = ec != null ? ec.Wrap() : null;
                }
                return _Cells;
            }
        }

        public override string Coordinates
        {
            get { return CellReferenceResult != null ? CellReferenceResult.Coordinates : null; }
        }

        public CellReference CellReferenceResult
        {
            get { return CellReferenceListResult.Count == 0 ? null : CellReferenceListResult[0]; }
        }

        public override string Value
        {
            get { return Result; }
        }

        public virtual string Result
        {
            get
            {
                if (!_ExprValueCalculated)
                {
                    _ExprValueCalculated = true;
                    _ExprValue = EvaluateExpression();
                }

                return _ExprValue;
            }
        }

        protected virtual string EvaluateExpression()
        {
            if (Owner == null)
            {
                throw new InvalidOperationException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.EddFirstActiveCellAsOwner));
            }
            if (CellReferenceListResult.Count == 0)
            {
                throw new UnrecoverableAppException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.EddInvalidCellReference),
                        CoordExpression, ExpressionSource));
            }

            return
                CellReferenceListResult.Select(cr => Owner.DataSource.GetCellValue(cr.Coordinates, cr.SheetName))
                    .FirstOrDefault(val => !string.IsNullOrEmpty(val));
        }

        protected internal override void Reset()
        {
            base.Reset();
            //if( IsExpression ) {
            _ExprValueCalculated = false;
            _Cells = null;
            //}
        }
    }

    public class CellReferenceListExpression : CellReferenceExpression
    {
        public readonly IIndexer<CellReferenceExpression> Expressions;

        public CellReferenceListExpression(ICollection<string> coordExpresssions, string exprSource,
            bool isExpression = false)
            : base(coordExpresssions.First(), exprSource, isExpression)
        {
            Expressions =
                new Indexer<CellReferenceExpression>(
                    coordExpresssions.Select(cex => new CellReferenceExpression(cex, exprSource, IsExpression)));
            if (!isExpression)
            {
                EvaluationSource = string.Join(";", Expressions.Select(exp => exp.EvaluationSource));
            }
        }

        protected internal override void Reset()
        {
            base.Reset();
            for (var ii = 0; ii < Expressions.Count; ii++)
            {
                var exp = Expressions[ii];
                exp.Reset();
            }
        }

        protected override CellReference[] EvaluateCoordinates()
        {
            var rl = new List<CellReference>();
            for (var ii = 0; ii < Expressions.Count; ii++)
            {
                var exp = Expressions[ii];
                exp.Owner = Owner;
                rl.AddRange(exp.CellReferenceListResult);
            }
            var res = InterpretExpressionResult(Owner, rl.ToArray());
            EvaluationSource = string.Join(";", res.Select(ex => ex.FullCoordinates));
            return res;
        }

        public override ExcelCellIterator Owner
        {
            get { return base.Owner; }
            internal set
            {
                if (Owner == value)
                {
                    return;
                }
                base.Owner = value;
                for (var ii = 0; ii < Expressions.Count; ii++)
                {
                    var exp = Expressions[ii];
                    exp.Owner = Owner;
                }
            }
        }
    }

    public class BreakdownDimensionExpression : StringExpression
    {
        public readonly string[] BreakdownDimensions;
        private readonly Code[] _CurrentBreakdownValues;

        public BreakdownDimensionExpression(string workbookNameExpr, string exprSource,
            params string[] breakdownDimensions)
            : base(workbookNameExpr, exprSource, true)
        {
            if (breakdownDimensions == null || breakdownDimensions.Length == 0 ||
                breakdownDimensions.Any(string.IsNullOrWhiteSpace))
            {
                throw new ApplicationArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.EddBreakdownNonEmpty),
                    exprSource));
            }


            BreakdownDimensions = breakdownDimensions;
            _CurrentBreakdownValues = new Code[breakdownDimensions.Length];
        }

        internal void SetCurrentBreakdownValues(Code[] values)
        {
            throw new NotImplementedException();

            //for (int ii = 0; ii < BreakdownDimensions.Length; ii++)
            //{
            //    var dimCode = BreakdownDimensions[ii];
            //    var mem = Array.Find(values, m => m.Codelist.Code.IsSameAs(dimCode));

            //    if (mem == null)
            //    {
            //        throw new ApplicationArgumentException(
            //            ":-Invalid values supplied to SetCurrentBreakdownValues() function");
            //    }

            //    _CurrentBreakdownValues[ii] = mem;
            //}

            //Reset();
        }

        //private Dimension GetDimension(int index)
        //{
        //    if (index <= 0 || index > _CurrentBreakdownValues.Length)
        //    {
        //        throw new ApplicationArgumentException("Call to GetDimension() with invalid index {0} detected in expression {1}"
        //            .F(
        //                index,
        //                ValueExpression));
        //    }

        //    var dim = Owner.CoordDescriptor.Dataflow.FindDimension(BreakdownDimensions[index - 1]);

        //    if (dim == null)
        //    {
        //        throw new ApplicationArgumentException(
        //            "Invalid dimension code {0} provided in breakdown dimensions list in expression {1}".F(
        //                BreakdownDimensions[index - 1],
        //                ValueExpression));
        //    }

        //    return dim;
        //}

        //public override string BreakdownMemberCode(int index)
        //{
        //    if (index <= 0 || index > _CurrentBreakdownValues.Length)
        //    {
        //        throw new ApplicationArgumentException("Call to GetMemberCode() with invalid index {0} detected in expression {1}"
        //            .F(
        //                index,
        //                ValueExpression));
        //    }

        //    return _CurrentBreakdownValues[index - 1].Code;
        //}
    }

    public class StringExpression : ExpresssionBase
    {
        public readonly bool IsExpression;
        public readonly string ValueExpression;
        private Func<StringExpression, object> _ValueFunc;
        private bool _ValueCalculated;
        private string _Value;
        private string[] _Values;


        public StringExpression(string valueExpression, string exprSource, bool isExpression = true)
            : base(null, exprSource)
        {
            ValueExpression = valueExpression;
            IsExpression = isExpression;

            if (!IsExpression)
            {
                _Value = valueExpression;
                _ValueCalculated = true;
                EvaluationSource = valueExpression;
            }
        }

        protected virtual string[] EvaluateValues()
        {
            if (_ValueFunc == null)
            {
                _ValueFunc = BuildFunc<StringExpression, object>(ValueExpression);
            }

            object res;
            try
            {
                res = _ValueFunc(this);
            }
            catch (UnrecoverableAppException ex)
            {
                throw new UnrecoverableAppException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.EddExpressionEvaluation),
                        ValueExpression, ExpressionSource, ex.Message),
                    ex);
            }
            catch (System.Exception ex)
            {
                throw new ApplicationArgumentException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.EddExpressionEvaluation),
                        ValueExpression, ExpressionSource, ex.Message),
                    ex);
            }


            if (res == null)
            {
                return null;
            }

            var str = res as string;

            if (str != null)
            {
                return new[] {str};
            }

            var strl = res as IEnumerable<string>;
            string[] sta = null;

            if (strl != null)
            {
                sta = strl.ToArray();
            }

            if (sta == null || sta.Length == 0)
            {
                throw new ApplicationArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.EddStringCollectionExpression),
                    ValueExpression));
            }

            return sta;
        }

        public virtual string Result
        {
            get
            {
                if (Owner == null)
                {
                    throw new InvalidOperationException(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.EddFirstActiveCellAsOwner));
                }

                if (!_ValueCalculated)
                {
                    _ValueCalculated = true;
                    _Values = EvaluateValues();
                    _Value = _Values == null ? null : _Values[0];
                }

                return _Value;
            }
        }

        public virtual IEnumerable<string> Results
        {
            get
            {
                if (!_ValueCalculated)
                {
                    _ValueCalculated = true;
                    _Values = EvaluateValues();
                    _Value = _Values[0];
                }

                return _Values;
            }
        }

        protected internal override void Reset()
        {
            base.Reset();
            if (IsExpression)
            {
                _ValueCalculated = false;
                _Value = null;
            }
        }
    }

    public class ValueCellReferenceExpression : ExpresssionBase
    {
        public readonly string CoordExpression;
        public readonly string ValueExpression;
        public readonly bool IsExpression;
        public readonly string MemberMapperName;

        private Func<ValueCellReferenceExpression, object> _ValueFunc;

        private string _ExprValue;
        private bool _ExprValueCalculated;

        private CellReferenceExpression _CellRefResult;

        public ValueCellReferenceExpression(string valueExpression,
            string coordExpression,
            string exprSource,
            bool isExpression,
            string memberMapperName = null)
            : base(
                string.IsNullOrWhiteSpace(coordExpression) || isExpression
                    ? null
                    : CellReference.ExtractSheetName(coordExpression),
                exprSource)
        {
            if (string.IsNullOrWhiteSpace(valueExpression) && string.IsNullOrWhiteSpace(coordExpression))
            {
                throw new ApplicationArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.EddValueOrCoordinateExpression),
                    exprSource));
            }

            MemberMapperName = memberMapperName;
            IsExpression = isExpression;
            _ExprValueCalculated = false;
            ValueExpression = string.IsNullOrWhiteSpace(valueExpression) ? null : valueExpression.Trim();
            CoordExpression = string.IsNullOrWhiteSpace(coordExpression) ? null : coordExpression.Trim();

            if (ValueExpression != null && !isExpression)
            {
                _ExprValue = valueExpression;
                _ExprValueCalculated = true;
                EvaluationSource = ValueExpression;
            }

            if (CoordExpression != null && !isExpression)
            {
                _CellRefResult = new CellReferenceExpression(CoordExpression, exprSource);
                EvaluationSource = CoordExpression;
            }
        }

        public override string Coordinates
        {
            get { return CellReferenceResult != null ? CellReferenceResult.Coordinates : null; }
        }

        public virtual CellReference CellReferenceResult
        {
            get
            {
                if (CoordExpression == null)
                {
                    return null;
                }

                if (_CellRefResult == null)
                {
                    _CellRefResult = new CellReferenceExpression(CoordExpression, ExpressionSource, true)
                    {
                        Owner = Owner
                    };
                    EvaluationSource = _CellRefResult.EvaluationSource;
                }

                return _CellRefResult.CellReferenceResult;
            }
        }

        protected virtual string EvaluateExpression()
        {
            try
            {
                if (Owner == null)
                {
                    throw new InvalidOperationException(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.EddFirstActiveCellAsOwner));
                }

                if (ValueExpression == null)
                {
                    EvaluationSource = CoordExpression;
                    if (CellReferenceResult == null)
                    {
                        throw new UnrecoverableAppException(string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId.EddInvalidCellReference),
                            CoordExpression, ExpressionSource));
                    }
                    return Owner.DataSource.GetCellValue(CellReferenceResult.Coordinates, CellReferenceResult.SheetName);
                }
                EvaluationSource = ValueExpression;

                if (_ValueFunc == null)
                {
                    _ValueFunc = BuildFunc<ValueCellReferenceExpression, object>(ValueExpression);
                }

                object res;
                try
                {
                    res = _ValueFunc(this);
                }
                catch (UnrecoverableAppException ex)
                {
                    throw new UnrecoverableAppException(string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId.EddExpressionEvaluation),
                            ValueExpression, ExpressionSource, ex.Message),
                        ex);
                }
                catch (System.Exception ex)
                {
                    throw new ApplicationArgumentException(string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId.EddExpressionEvaluation),
                            ValueExpression, ExpressionSource, ex.Message),
                        ex);
                }

                if (res != null)
                {
                    var str = res as string;
                    if (str == null)
                    {
                        if (res.IsNumeric())
                        {
                            str = res.ToString();
                        }
                    }
                    if (str == null)
                    {
                        throw new ApplicationArgumentException(string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId.EddStringExpression),
                            ValueExpression, ExpressionSource));
                    }

                    return str;
                }
                return null;
            }
            catch (ParseException ex)
            {
                throw new ApplicationArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.EddParceError),
                    ValueExpression, ExpressionSource));
            }
        }

        public virtual string Result
        {
            get
            {
                if (!_ExprValueCalculated)
                {
                    _ExprValueCalculated = true;
                    _ExprValue = EvaluateExpression();
                }

                return _ExprValue;
            }
        }

        protected internal override void Reset()
        {
            base.Reset();

            if (_CellRefResult != null)
            {
                _CellRefResult.Reset();
            }

            //if( IsExpression ) {
            _ExprValue = null;
            _ExprValueCalculated = false;
            //}
        }

        public override ExcelCellIterator Owner
        {
            get { return base.Owner; }

            internal set
            {
                if (Owner == value)
                {
                    return;
                }
                base.Owner = value;

                if (_CellRefResult != null)
                {
                    _CellRefResult.Owner = value;
                }
            }
        }
    }

    public class SdmxArtifactCellReferenceExpression : ValueCellReferenceExpression
    {
        public int Index { get; private set; }
        public readonly string Code;
        private readonly List<string> _ValueExpression;
        private readonly CellReferenceListExpression _CellReferenceList;

        private readonly List<Func<SdmxArtifactCellReferenceExpression, object>> _ValueExpressionFuncs;

        public SdmxArtifactCellReferenceExpression(string code,
            IList<string> valueExpression,
            IList<string> coordExpression,
            string exprSource,
            bool isExpression = true)
            : base(
                valueExpression != null && valueExpression.Count > 0 ? valueExpression[0] : null,
                coordExpression != null && coordExpression.Count > 0 ? coordExpression[0] : null,
                exprSource,
                isExpression,
                null)
        {
            if (string.IsNullOrWhiteSpace(code))
            {
                throw new ApplicationArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.EddSpecifyCode),
                    exprSource)); 
            }

            if ((valueExpression == null || valueExpression.Count == 0) &&
                (coordExpression == null || coordExpression.Count == 0))
            {
                throw new ApplicationArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.EddValueOrCoordinateExpression),
                    exprSource));
            }

            Code = code;
            _ValueExpression = new List<string>(valueExpression ?? Enumerable.Empty<string>());

            _CellReferenceList = coordExpression != null && coordExpression.Count > 0
                ? new CellReferenceListExpression(coordExpression, exprSource, isExpression)
                : null;
            _ValueExpressionFuncs = new List<Func<SdmxArtifactCellReferenceExpression, object>>();
        }

        protected override string EvaluateExpression()
        {
            if (Owner == null)
            {
                throw new InvalidOperationException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.EddFirstActiveCellAsOwner));
            }

            EvaluationSource = string.Join(";", _ValueExpression);

            for (var ii = 0; ii < _ValueExpression.Count; ii++)
            {
                Func<SdmxArtifactCellReferenceExpression, object> func;

                if (_ValueExpressionFuncs.Count <= ii)
                {
                    var ve = _ValueExpression[ii];
                    func = IsExpression ? BuildFunc<SdmxArtifactCellReferenceExpression, object>(ve) : cv => ve;
                    _ValueExpressionFuncs.Add(func);
                }
                else
                {
                    func = _ValueExpressionFuncs[ii];
                }


                string res;
                try
                {
                    var fres = func(this);
                    res = fres != null ? Convert.ToString(fres) : null;
                }
                catch (DuplicateNameException)
                {
                    throw;
                }
                catch (InvalidCellReferenceException)
                {
                    res = null;
                }
                catch (UnrecoverableAppException ex)
                {
                    throw new UnrecoverableAppException(string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId.EddExpressionEvaluation),
                            _ValueExpression[ii], ExpressionSource, ex.Message),
                        ex);
                }
                catch (System.Exception ex)
                {
                    throw new ApplicationArgumentException(string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId.EddExpressionEvaluation),
                            _ValueExpression[ii], ExpressionSource, ex.Message),
                        ex);
                }

                if (!string.IsNullOrWhiteSpace(res))
                {
                    return res;
                }
            }

            if (_CellReferenceList != null)
            {
                EvaluationSource = _CellReferenceList.EvaluationSource;
                return _CellReferenceList.Result;
            }
            EvaluationSource = "Unknown";
            return null;
        }

        public IIndexer<CellReferenceExpression> CellReferenceListResult
        {
            get
            {
                return _CellReferenceList != null
                    ? _CellReferenceList.Expressions
                    : new Indexer<CellReferenceExpression>(Enumerable.Empty<CellReferenceExpression>());
            }
        }

        public IIndexer<string> ValueExpressionList
        {
            get { return _ValueExpression.Wrap(); }
        }

        protected internal override void Reset()
        {
            base.Reset();

            if (_CellReferenceList != null)
            {
                _CellReferenceList.Reset();
                _CellReferenceList.Reset();
            }
        }

        public override ExcelCellIterator Owner
        {
            get { return base.Owner; }

            internal set
            {
                if (Owner == value)
                {
                    return;
                }
                base.Owner = value;
                if (_CellReferenceList != null)
                {
                    _CellReferenceList.Owner = value;
                }
            }
        }

        public void Init(int index, ExcelCellIterator owner)
        {
            Index = index;
            Owner = owner;
        }
    }

}
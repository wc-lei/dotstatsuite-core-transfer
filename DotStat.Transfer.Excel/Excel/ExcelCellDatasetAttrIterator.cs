﻿using System;
using System.Collections.Generic;
using DotStat.Common.Localization;
using DotStat.Domain;
using DotStat.Transfer.Excel.Mapping;
using DotStat.Transfer.Excel.Reader;
using Org.Sdmxsource.Sdmx.Api.Model.Data;

namespace DotStat.Transfer.Excel.Excel
{
    public sealed class ExcelCellDatasetAttrIterator : ExcelCellIterator, IRecordIterator<IKeyValue>
    {
        private readonly IEnumerator<DatasetAttributeLookup> _attributeEnumerator; 
        private readonly DatasetAttrMapper _mapper;

        public ExcelCellDatasetAttrIterator(ExcelDatasetAttributesDescriptor descriptor, Dataflow dataflow, IExcelDataSource dataSource) : base(dataSource, descriptor.Worksheet)
        {
            _mapper = new DatasetAttrMapper(dataflow);
            _attributeEnumerator = descriptor.DatasetAttributeLookups.GetEnumerator();

            BuildFieldList();
        }

        public override string Name => null;

        V8Mapper<IKeyValue> IRecordIterator<IKeyValue>.Mapper => _mapper;
        public override SWMapper Mapper => _mapper;

        protected override CellReference GetNextDataCell()
        {
            if (!_attributeEnumerator.MoveNext() || _attributeEnumerator.Current == null)
                return null;

            return new CellReference(_attributeEnumerator.Current.Coord)
            {
                Owner = this
            };
        }

        public override string GetField(int pos)
        {
            if(_attributeEnumerator.Current == null)
                throw new InvalidOperationException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelNullDatasetAttribute));

            switch (pos)
            {
                case 0:
                    return _attributeEnumerator.Current.Code;

                case 1:
                    return Current.Value;

                default:
                    throw new InvalidOperationException(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelUnknownIndex));
            }
        }


        /// <summary>
        /// code/index maping: value, dimensions, observation attributes
        /// </summary>
        private void BuildFieldList()
        {
            for (var i = 0; i < _mapper.SourceNames.Count; i++)
            {
                _FieldsByName.Add(_mapper.SourceNames[i], i);
                _FieldsById.Add(i, _mapper.SourceNames[i]);
            }
        }
    }
}

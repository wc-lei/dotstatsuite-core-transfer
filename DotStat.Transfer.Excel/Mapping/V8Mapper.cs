﻿using System;
using System.Collections.Generic;
using System.Linq;
using DotStat.Common.Localization;
using DotStat.Db.Exception;
using DotStat.Db.Validation;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
using Attribute = DotStat.Domain.Attribute;

namespace DotStat.Transfer.Excel.Mapping
{
    public abstract class V8Mapper<T> : SWMapper
    {
        protected V8Mapper(Dataflow targetDataset,  IEnumerable<string> sourceNames = null) : base(targetDataset, sourceNames: sourceNames)
        {}

        public new abstract T Transform(string[] source);

        protected IKeyValue GetKeyValue(IDotStatCodeListBasedIdentifiable item,  string value)
        {
            if (!string.IsNullOrEmpty(value) && item.Codelist != null)
            {
                var clMember = item.FindMember(value);

                if (clMember == null)
                    throw new KeyValueReadException(ValidationErrorType.UnknownCodeMember, null, item.Code, value);

                value = clMember.Code;
            }

            return new KeyValueImpl(value, item.Code);
        }
    }

    public class ObservationMapper : V8Mapper<IObservation>
    {
        public ObservationMapper(Dataflow targetDataset) : base(targetDataset)
        {}

        public override IObservation Transform(string[] source)
        {
            var dimensions = TargetDataset.Dsd.Dimensions;
            var obsAttributes = TargetDataset.ObsAttributes.ToArray();
            var timeDim = dimensions.First(d => d.Base.TimeDimension);

            var value = source[0];
            var time = source[timeDim.Index + 1];
            var seriesKey = new List<IKeyValue>(dimensions.Count - 1);
            var attrList = new List<IKeyValue>(obsAttributes.Length);

            // not-time dimensions as series key
            foreach (var dim in dimensions.Where(d => !d.Base.TimeDimension))
            {
                var key = source[dim.Index + 1];

                if (string.IsNullOrEmpty(key))
                    throw new ObservationReadException(ValidationErrorType.MissingCodeMember, null, dim.Code);

                var clMember = dim.FindMember(key);

                if (clMember == null)
                    throw new ObservationReadException(ValidationErrorType.UnknownCodeMember, null, dim.Code + ":" + key);

                seriesKey.Add(new KeyValueImpl(clMember.Code, dim.Code));
            }

            for (var index = 0; index < obsAttributes.Length; index++)
            {
                var attr = obsAttributes[index];
                var key = source[dimensions.Count + index + 1];

                if (!string.IsNullOrEmpty(key) && attr.Codelist != null)
                {
                    var clMember = attr.FindMember(key);

                    if (clMember == null)
                        throw new ObservationReadException(ValidationErrorType.UnknownCodeMember, null, attr.Code + ":" + key);

                    key = clMember.Code;
                }

                attrList.Add(new KeyValueImpl(key, attr.Code));
            }

            var observation =
                new ObservationImpl(new KeyableImpl(TargetDataset.Base, TargetDataset.Dsd.Base, seriesKey, null),
                    time,
                    value,
                    attrList
                 );

            return observation;
        }
    }

    public class DatasetAttrMapper : V8Mapper<IKeyValue>
    {
        private readonly IList<Attribute> _attributes;
        

        public DatasetAttrMapper(Dataflow targetDataset) : base(targetDataset, new[] {"ATTRIBUTE", "VALUE"})
        {
            _attributes = TargetDataset.Dsd.Attributes.Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.DataSet).ToArray();
        }

        public override IKeyValue Transform(string[] source)
        {
            var attrCode    = source[0];
            var attrValue   = source[1];
            var attribute   = _attributes.FirstOrDefault(a => a.Code == attrCode);
         
            if(attribute == null)
                throw new KeyValueReadException(ValidationErrorType.AttributeNotInDsd, null, attrCode, TargetDataset.FullId);

            return GetKeyValue(attribute, attrValue);
        }
    }

    public class GroupAttrMapper : V8Mapper<IKeyable>
    {
        private static readonly List<AttributeAttachmentLevel> Filter = new List<AttributeAttachmentLevel>()
        {
            AttributeAttachmentLevel.Group,
            AttributeAttachmentLevel.DimensionGroup
        };

        private readonly Dictionary<string, GroupAttribute> _attributeMap;
        
        public GroupAttrMapper(Dataflow targetDataset) : base(targetDataset, GetColumns(targetDataset))
        {
            _attributeMap = new Dictionary<string, GroupAttribute>(StringComparer.InvariantCultureIgnoreCase);
            var groups = TargetDataset.Dsd.Base.Groups;

            var dimMap = SourceNames.Select((s, i) => new KeyValuePair<string, int>(s, i))
                            .Where(p => p.Value >= 2)
                            .ToDictionary(x => x.Key, x => new KeyValuePair<int, Dimension>(x.Value, TargetDataset.Dimensions.First(dim => dim.Code == x.Key)));

            foreach (var attribute in TargetDataset.Dsd.Attributes.Where(a => Filter.Contains(a.Base.AttachmentLevel)))
            {
                var attributeDimensions = attribute.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup
                    ? attribute.Base.DimensionReferences
                    : groups.First(g => g.Id.Equals(attribute.Base.AttachmentGroup)).DimensionRefs;

                var dimensions = (from dimCode in attributeDimensions let dimension = dimMap[dimCode] select new DimIndex(dimension.Key, dimension.Value)).ToArray();
                var groupAttribute = new GroupAttribute(attribute, dimensions);

                _attributeMap.Add(attribute.Code, groupAttribute);
            }
        }

        private static IEnumerable<string> GetColumns(Dataflow dataflow)
        {
            yield return "ATTRIBUTE";
            yield return "VALUE";

            foreach (var dim in dataflow.Dimensions)
                if (!dim.Base.TimeDimension)
                    yield return dim.Code;
        }

        public override IKeyable Transform(string[] source)
        {
            if (source.Length != SourceCount)
                throw new InvalidOperationException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.V8MapperWrongInputArray),
                    source.Length,
                    SourceCount)
                );

            GroupAttribute attribute;

            var attrCode = source[0];
            var attrValue = source[1];

            if (string.IsNullOrEmpty(attrCode))
                throw new InvalidOperationException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.V8MapperNullAttribute));

            if (!_attributeMap.TryGetValue(attrCode, out attribute))
                throw new KeyValueReadException(ValidationErrorType.AttributeNotInDsd, null, attrCode, TargetDataset.FullId);

            var attrKeyValue = GetKeyValue(attribute.Attribute, attrValue);
            var dimensionKeys = attribute.Dimensions.Select(x => GetKeyValue(x.Dimension, source[x.Index])).ToArray();

            return new KeyableImpl(
                TargetDataset.Base, 
                TargetDataset.Dsd.Base, 
                dimensionKeys, 
                new List<IKeyValue>() { attrKeyValue }, 
                groupName: attribute.Attribute.Base.AttachmentGroup
            );
        }

        private class GroupAttribute
        {
            public readonly Attribute Attribute;
            public readonly IList<DimIndex> Dimensions;

            public GroupAttribute(Attribute attribute, IList<DimIndex> dimensions)
            {
                Attribute = attribute;
                Dimensions = dimensions;
            }
        }

        private class DimIndex
        {
            public readonly int Index;
            public readonly Dimension Dimension;

            public DimIndex(int index, Dimension dimension)
            {
                Index = index;
                Dimension = dimension;
            }
        }
    }
}

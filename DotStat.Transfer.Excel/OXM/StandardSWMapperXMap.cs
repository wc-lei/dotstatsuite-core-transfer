﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using DotStat.Common.Localization;
using DotStat.Domain;
using DotStat.Transfer.Excel.Exceptions;
using DotStat.Transfer.Excel.Mapping;
using DotStat.Transfer.Excel.Util;
using OXM;

namespace DotStat.Transfer.Excel.OXM
{
    internal class NamedMappingItemListXMap : ClassMap<NamedMappingItemList>
    {
        private readonly List<NamedMappingItemList.MappingItem> _Members;
        private string _Name;

        public NamedMappingItemListXMap()
        {
            _Members = new List<NamedMappingItemList.MappingItem>();

            Map(m => m.Name)
                .ToAttribute("Name", true)
                .Set(v => _Name = v)
                .Converter(new StringConverter());
            MapCollection(m => m.Mappings)
                .ToElement(XName.Get("Member", StandardSWMapperXMap.MappingXmlNamespace), false)
                .Set(v => _Members.Add(v))
                .ClassMap(() => new NamedMappingItemXMap());
        }

        protected override NamedMappingItemList Return()
        {
            if (string.IsNullOrWhiteSpace(_Name))
            {
                throw new ApplicationArgumentException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelMappingNameNoProvided)
                );
            }
            var res = new NamedMappingItemList(null, _Name);
            foreach (var mi in _Members)
            {
                res.AddMapping(mi.ExternMember, mi.SWMember, mi.IsRegex);
            }
            return res;
        }
    }

    internal class NamedMappingItemXMap : ClassMap<NamedMappingItemList.MappingItem>
    {
        private string _DimensionMemberCode;
        private string _ExternMember;
        private bool _IsRegex;

        public NamedMappingItemXMap()
        {
            Map(m => m.ExternMember)
                .ToAttribute("ExternMember", true)
                .Set(externMem => _ExternMember = externMem)
                .Converter(new StringConverter());

            Map(m => m.SWMember)
                .ToAttribute("Code", true)
                .Set(code => _DimensionMemberCode = code)
                .Converter(new StringConverter());

            Map(m => m.IsRegex)
                .ToAttribute("IsRegex", false, "false")
                .Set(isregex => _IsRegex = isregex)
                .Converter(new BooleanConverter());
        }

        protected override NamedMappingItemList.MappingItem Return()
        {
            if (string.IsNullOrWhiteSpace(_ExternMember))
            {
                throw new ApplicationArgumentException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelExternalMemberNoSpecified)
                );
            }
            return new NamedMappingItemList.MappingItem(0, _DimensionMemberCode, _ExternMember, _IsRegex);
        }
    }


    internal class ExternMemberSWDimensionMemberPairXMap : ClassMap<NamedMappingItemList.MappingItem>
    {
        private string _DimensionMemberCode;
        private int _DimensionMemberId;
        private string _ExternMember;
        private bool _IsRegex;

        public ExternMemberSWDimensionMemberPairXMap()
        {
            Map(m => m.ExternMember)
                .ToAttribute("ExternMember", true)
                .Set(externMem => _ExternMember = externMem)
                .Converter(new StringConverter());

            Map(m => m.SWMember)
                .ToAttribute("Code", true)
                .Set(code => _DimensionMemberCode = code)
                .Converter(new StringConverter());

            Map(m => m.SWMemberId)
                .ToAttribute("Id", false)
                .Set(v => _DimensionMemberId = v)
                .Converter(new Int32Converter());

            Map(m => m.IsRegex)
                .ToAttribute("IsRegex", false, "false")
                .Set(isregex => _IsRegex = isregex)
                .Converter(new BooleanConverter());
        }

        protected override NamedMappingItemList.MappingItem Return()
        {
            return new NamedMappingItemList.MappingItem(_DimensionMemberId, _DimensionMemberCode, _ExternMember,
                _IsRegex);
        }
    }

    internal class ExternDimensionSWDimensionPairXMap : ClassMap<ExternDimensionSWDimensionPair>
    {
        private readonly List<NamedMappingItemList.MappingItem> _Mappings;
        private readonly DimensionPairContainerXMap _Owner;

        private string _DimensionCode;
        private string _DimensionId;
        private string _ExternDimension;
        private string _FixedExternMemberCode;
        private string _FixedMemberCode;
        private string _FixedMemberId;
        private string _NamedMappingRef;

        public ExternDimensionSWDimensionPairXMap(DimensionPairContainerXMap owner)
        {
            _Owner = owner;

            _Mappings = new List<NamedMappingItemList.MappingItem>();

            Map(GetDimensionForOxm)
                .ToAttribute("Code", false)
                .Set(v => _DimensionCode = v)
                .Converter(new StringConverter());
            Map(GetDimensionIdForOxm)
                .ToAttribute("Id", false)
                .Set(v => _DimensionId = v)
                .Converter(new StringConverter());

            Map(GetExternDimensionForOxm)
                .ToAttribute("ExternDimension", false)
                .Set(v => _ExternDimension = v)
                .Converter(new StringConverter());

            Map(m => m.FixedMember)
                .ToAttribute("FixedMemberCode", false)
                .Set(v => _FixedMemberCode = v)
                .Converter(new StringConverter());

            Map(GetFixedDimensionMemberIdForOxm)
                .ToAttribute("FixedMemberId", false)
                .Set(v => _FixedMemberId = v)
                .Converter(new StringConverter());

            Map(m => m.FixedExternMember)
                .ToAttribute("FixedExternMember", false)
                .Set(v => _FixedExternMemberCode = v)
                .Converter(new StringConverter());

            Map(m => m.MappingItemRefName)
                .ToAttribute("MemberMappingRef", false)
                .Set(v => _NamedMappingRef = v)
                .Converter(new StringConverter());

            MapCollection(m => m.Mappings)
                .ToElement("Member", false)
                .Set(AddMapping)
                .ClassMap(() => new ExternMemberSWDimensionMemberPairXMap());
        }

        private void AddMapping(NamedMappingItemList.MappingItem mi)
        {
            if (string.IsNullOrWhiteSpace(mi.ExternMember) || string.IsNullOrWhiteSpace(mi.SWMember) ||
                _Mappings.Contains(mi))
            {
                return;
            }
            _Mappings.Add(mi);
        }

        private static string GetDimensionForOxm(ExternDimensionSWDimensionPair pair)
        {
            if (pair.Dimension.IsSameAs(Dataflow.DefaultValueField))
            {
                return null;
            }
            return pair.Dimension;
        }

        private Dimension GetSWDimensionForOxm(ExternDimensionSWDimensionPair pair)
        {
            throw new NotImplementedException();
            //string dim = GetDimensionForOxm( pair );
            //if( string.IsNullOrEmpty(dim) ) {
            //    return null;
            //}
            //var ds = _Owner.Owner.GetDataset( );
            //if( ds == null ) {
            //    return null;
            //}
            //var dsd = ds.FindDimension( dim, DbLanguage.Code );
            //if( dsd == null ) {
            //    return null;
            //}
            //return dsd;
        }

        private string GetDimensionIdForOxm(ExternDimensionSWDimensionPair pair)
        {
            var dim = pair.SWDimension ?? GetSWDimensionForOxm(pair);
            return dim?.DbId.ToString();
        }

        private string GetFixedDimensionMemberIdForOxm(ExternDimensionSWDimensionPair pair)
        {
            throw new NotImplementedException();
            //var dim = pair.Dimension?? GetSWDimensionForOxm( pair );
            //var mem = dim?.FindMember( pair.FixedMember, DbLanguage.Code );
            //return mem?.Id.ToString( );
        }

        private static string GetExternDimensionForOxm(ExternDimensionSWDimensionPair pair)
        {
            if (GetDimensionForOxm(pair)
                .IsSameAs(pair.ExternDimension))
            {
                return null;
            }
            return pair.ExternDimension;
        }


        protected override ExternDimensionSWDimensionPair Return()
        {
            throw new NotImplementedException();

            /*
            var ds = _Owner.Owner.GetDataset();
            Dimension dim = null;
            if (!string.IsNullOrWhiteSpace(_DimensionId))
            {
                //this code protects against code changes
                int dimid;
                if (int.TryParse(_DimensionId, out dimid))
                {
                    dim = ds.FindDimension(dimid);
                }
            }
            if (dim == null && !string.IsNullOrWhiteSpace(_DimensionCode))
            {
                dim = ds.FindDimension(_DimensionCode);
                if (dim == null)
                {
                    return null;
                    //throw new ApplicationArgumentException( "Invalid dimension code {0} is specified".F( _DimensionCode ) );
                }
                if (!string.IsNullOrEmpty(_FixedMemberCode))
                {
                    var fixedMember = dim.FindMember(_FixedMemberCode, DbLanguage.Code);
                    if (fixedMember == null)
                    {
                        throw new ApplicationArgumentException("The dimension {0} does not contain a member {1}".F(dim.FullCode, _FixedMemberCode));
                    }
                }
            }

            if (_FixedExternMemberCode == string.Empty && !string.IsNullOrEmpty(_FixedMemberCode))
            {
                _FixedExternMemberCode = null;
            }

            int memid;
            if (dim != null && !string.IsNullOrWhiteSpace(_FixedMemberId) && int.TryParse(_FixedMemberId, out memid))
            {
                var mem = dim.FindMember(memid);
                if (mem != null)
                {
                    _FixedMemberCode = mem.Code;
                }
            }

            if (_FixedMemberCode == string.Empty && !string.IsNullOrEmpty(_FixedExternMemberCode))
            {
                _FixedMemberCode = null;
            }


            if (!string.IsNullOrWhiteSpace(_NamedMappingRef) && !string.IsNullOrWhiteSpace(_FixedMemberCode) && string.IsNullOrWhiteSpace(_FixedExternMemberCode))
            {
                throw new ApplicationArgumentException("When a fixed member is defined a reference to a named mapping list cannot be specified");
            }

            var res = new ExternDimensionSWDimensionPair(dim, _DimensionCode, _ExternDimension, _FixedMemberCode, _FixedExternMemberCode, _NamedMappingRef);

            if (dim != null)
            {
                foreach (var pair in _Mappings)
                {
                    var mem = pair.SWMember;
                    if (pair.SWMemberId != 0)
                    {
                        //this code protects against code changes
                        var swmem = dim.FindMember(pair.SWMemberId);
                        if (swmem != null)
                        {
                            mem = swmem.Code;
                        }
                    }
                    res.AddMapping(pair.ExternMember, mem, pair.IsRegex);
                }
            }
            else if (_Mappings.Any())
            {
                throw new ApplicationArgumentException("When a fixed member is defined a reference to a mapping list cannot be specified");
            }
            return res;
            */
        }
    }

    internal class ValueMappingPairXMap : ClassMap<Mapper.MappingElement>
    {
        private string _ExternCC;
        private string _ExternValue;
        private bool _IsRegex;
        private string _StatworksCC;
        private string _StatworksValue;


        public ValueMappingPairXMap()
        {
            Map(vm => vm.Projection[0])
                .ToAttribute("Value", false)
                .Set(sv => _StatworksValue = sv)
                .Converter(new StringConverter());

            Map(vm => vm.Projection[1])
                .ToAttribute("ControlCode", false)
                .Set(cc => _StatworksCC = cc)
                .Converter(new StringConverter());

            Map(vm => vm.KeySource[0])
                .ToAttribute("ExternValue", false)
                .Set(ev => _ExternValue = ev)
                .Converter(new StringConverter());

            Map(vm => vm.KeySource[1])
                .ToAttribute("ExternControlCode", false)
                .Set(ecc => _ExternCC = ecc)
                .Converter(new StringConverter());

            Map(vm => vm.IsRegEx)
                .ToAttribute("IsRegex", false, "false")
                .Set(ir => _IsRegex = ir)
                .Converter(new BooleanConverter());
        }

        protected override Mapper.MappingElement Return()
        {
            return new Mapper.MappingElement(-1, new[]
            {
                _ExternValue,
                _ExternCC
            },
                new[]
                {
                    _StatworksValue,
                    _StatworksCC
                },
                _IsRegex);
        }
    }

    internal class MappingItemXMap : ClassMap<Mapper.MappingElement>
    {
        private readonly MapperXMap _Owner;
        private bool _IsRegex;
        private string _KeySource;
        private string _Projection;

        public MappingItemXMap(MapperXMap owner)
        {
            _Owner = owner;
            Map(m => string.Join("|", m.KeySource)
                .TrimEnd('|'))
                .ToAttribute("ExternCoord", true)
                .Set(v => _KeySource = v)
                .Converter(new StringConverter());

            Map(m => string.Join("|", m.Projection)
                .TrimEnd('|'))
                .ToAttribute("Coord", true)
                .Set(v => _Projection = v)
                .Converter(new StringConverter());

            Map(m => m.IsRegEx)
                .ToAttribute("IsRegex", false, "false")
                .Set(v => _IsRegex = v)
                .Converter(new BooleanConverter());
        }

        protected override Mapper.MappingElement Return()
        {
            var src =
                _KeySource.Split(_Owner.ExternCoordSeparator[0])
                    .Select(v => v != null && v.Length == 0 ? null : v)
                    .ToArray();
            var proj =
                _Projection.Split(_Owner.ExternCoordSeparator[0])
                    .Select(v => v != null && v.Length == 0 ? null : v)
                    .ToArray();
            return new Mapper.MappingElement(-1, src, proj, _IsRegex);
        }
    }


    internal class ValueMapperXMap : ClassMap<Mapper>
    {
        private readonly Mapper _Result;

        internal ValueMapperXMap(StandardSWMapperXMap parent = null)
        {
            var cols = new[]
            {
                "V",
                "CC"
            };
            _Result = new Mapper(cols, cols, null);

            if (parent == null)
            {
                Map(m => m.Name)
                    .ToAttribute("Name", true)
                    .Set(v => _Result.Name = v)
                    .Converter(new StringConverter());
            }

            MapCollection(m => m.MappingItems)
                .ToElement(XName.Get("Map", StandardSWMapperXMap.MappingXmlNamespace), true)
                .Set(v => _Result.AddMapping(v.KeySource, v.Projection, v.IsRegEx))
                .ClassMap(() => new ValueMappingPairXMap());
        }

        protected override Mapper Return()
        {
            return _Result;
        }
    }

    internal class MapperXMap : ClassMap<Mapper>
    {
        internal readonly List<Mapper.MappingElement> MappingItems;
        private readonly StandardSWMapperXMap _Owner;
        internal string ExternCoordSeparator;
        private string _SourceNames;
        private string _TargetNames;
        private string _TargetTemplate;

        public MapperXMap(StandardSWMapperXMap owner)
        {
            MappingItems = new List<Mapper.MappingElement>();
            _Owner = owner;

            ExternCoordSeparator = "|";

            Map(m => string.Join("|", m.SourceNames)
                .TrimEnd('|'))
                .ToAttribute("ExternCoordFormat", false)
                .Set(v => _SourceNames = v)
                .Converter(new StringConverter());

            Map(m => string.Join("|", m.TargetNames)
                .TrimEnd('|'))
                .ToAttribute("CoordFormat", false)
                .Set(v => _TargetNames = v)
                .Converter(new StringConverter());

            Map(m => (string)null)
                .ToAttribute("ExternCoordSeparator", false, ExternCoordSeparator)
                .Set(v => ExternCoordSeparator = v)
                .Converter(new StringConverter());

            Map(BuildTargetTemplate)
                .ToAttribute("TargetTemplate", false)
                .Set(v => _TargetTemplate = v)
                .Converter(new StringConverter());

            MapCollection(m => m.MappingItems)
                .ToElement("Map", true)
                .Set(v => MappingItems.Add(v))
                .ClassMap(() => new MappingItemXMap(this));
        }

        protected override Mapper Return()
        {
            if (string.IsNullOrWhiteSpace(ExternCoordSeparator))
            {
                ExternCoordSeparator = _Owner.ExternFieldSeparator;
            }

            string[] sourceNames = GetSourceNames();
            string[] targetNames = GetTargetNames();

            var res = new Mapper(sourceNames, targetNames, GetMatchingNames(), GetTargetTemplate());
            foreach (var mi in MappingItems)
            {
                if (mi.KeySource.Length > sourceNames.Length || mi.Projection.Length > targetNames.Length)
                {
                    throw new ApplicationArgumentException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelWrongNumberOfCoordinates),
                        string.Join("|", mi.KeySource),
                        string.Join("|", mi.Projection))
                    );
                }
                res.AddMapping(mi.KeySource, mi.Projection, mi.IsRegEx);
            }
            return res;
        }

        private static string BuildTargetTemplate(Mapper m)
        {
            string res = string.Join("|", m.TargetTemplate)
                .TrimEnd('|');
            if (res.Length == 0)
            {
                return null;
            }
            return res;
        }

        private string[] GetSourceNames()
        {
            if (string.IsNullOrWhiteSpace(_SourceNames))
            {
                _SourceNames = _Owner.ExternFields;
                ExternCoordSeparator = _Owner.ExternFieldSeparator;
            }
            return _SourceNames.Split(ExternCoordSeparator[0]);
        }

        private string[] GetTargetTemplate()
        {
            if (_Owner != null)
            {
                //var ds = GetDataset( );
                //_Owner.CheckCoordMappings( ds, ExternCoordSeparator[ 0 ], _TargetNames );
                var targetNames = GetTargetNames();

                var targetTemplate = _TargetTemplate == null
                    ? new string[targetNames.Length]
                    : _TargetTemplate.Split(ExternCoordSeparator[0]);

                if (targetTemplate.Length != targetNames.Length)
                {
                    Array.Resize(ref targetTemplate, targetNames.Length);
                }

                foreach (var pair in _Owner.DimensionPairs.CoordMappings)
                {
                    if (pair.FixedMember != null)
                    {
                        var idx = targetNames.IndexOf(pair.Dimension, StringComparer.InvariantCultureIgnoreCase);
                        var tt = targetTemplate[idx];
                        if (tt != null && !tt.IsSameAs(pair.FixedMember))
                        {
                            throw new ApplicationArgumentException(string.Format(
                                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelNonmatchingFixedMemberAttribute),
                                pair.FixedMember, tt)
                            );
                        }
                        targetTemplate[idx] = pair.FixedMember;
                    }
                }
            }
            //value mapper
            return new string[2];
        }

        private string[] GetTargetNames()
        {
            if (string.IsNullOrWhiteSpace(_TargetNames))
            {
                return _Owner.GetDataset().GetColumnNames().ToArray();
            }
            return _TargetNames.Split(ExternCoordSeparator[0]);
        }

        private IEnumerable<Mapper.NameMatching> GetMatchingNames()
        {
            if (_Owner != null)
            {
                //var ds = GetDataset( );
                //_Owner.CheckCoordMappings( ds, _Owner.DimensionPairs.ExternDimensionSeparator[ 0 ], _TargetNames );

                return _Owner.DimensionPairs.CoordMappings.Where(cm => cm.FixedMember == null)
                    .Select(cm => new Mapper.NameMatching(cm.ExternDimension, cm.Dimension));
            }
            //value mapper
            //todo: this part should not be called, remove
            return new[]
            {
                new Mapper.NameMatching("V", "V"),
                new Mapper.NameMatching("CC", "CC")
            };
        }
    }

    public class DimensionPairContainer
    {
        protected internal readonly List<ExternDimensionSWDimensionPair> CoordMappings;

        internal DimensionPairContainer(StandardSWMapper mapper)
        {
            CoordMappings = mapper == null
                ? new List<ExternDimensionSWDimensionPair>()
                : mapper.CoordMappings.ToList();
        }
    }

    internal class DimensionPairContainerXMap : ClassMap<DimensionPairContainer>
    {
        internal readonly StandardSWMapperXMap Owner;
        private readonly DimensionPairContainer _Result;

        public DimensionPairContainerXMap(StandardSWMapperXMap owner)
        {
            Owner = owner;
            _Result = new DimensionPairContainer(null);


            MapCollection(m => m.CoordMappings)
                .ToElement("Dimension", false)
                .Set(fm => _Result.CoordMappings.Add(fm))
                .ClassMap(() => new ExternDimensionSWDimensionPairXMap(this));
        }

        protected override DimensionPairContainer Return()
        {
            return _Result;
        }
    }

    public class StandardSWMapperXMap : RootElementMap<StandardSWMapper>, IDatabaseDatasetRelated
    {
        internal const string MappingXmlNamespace = ""; // "http://oecd.org/StatWorks/Mapping.xsd";

        protected readonly double OverridePowerFactor;
        protected readonly IDatabaseDatasetRelated Owner;
        private readonly bool _ReversePowerFactor;
        protected Mapper AdvancedMappings;
        protected internal string DatabaseCode;
        protected internal string DatasetCode;
        protected int DecimalPlaces;

        protected internal DimensionPairContainer DimensionPairs;
        protected internal string ExternalControlCodesColumn;
        protected internal string ExternalValueColumn;
        protected internal string ExternFields;
        protected string MapperName;
        protected double PowerFactor;
        protected bool RetainTextFormatting;
        protected string SourceDataset;

        protected string QualitativeSeparator;
        internal Mapper ValueMappings;

        protected internal string ExternFieldSeparator { get; private set; }

        public StandardSWMapperXMap(IDatabaseDatasetRelated owner = null, double overridePowerFactor = 0.0,
            bool reversePowerFactor = false)
        {
            Owner = owner;
            OverridePowerFactor = overridePowerFactor;
            _ReversePowerFactor = reversePowerFactor;
            ExternFieldSeparator = "|";

            DimensionPairs = new DimensionPairContainer(null);

            Map(m => string.IsNullOrEmpty(m.Name)
                ? null
                : m.Name)
                .ToAttribute("Name", false)
                .Set(name => MapperName = name)
                .Converter(new StringConverter());

            Map(m => ExternFieldSeparator)
                .ToAttribute("ExternFieldSeparator", false, ExternFieldSeparator)
                .Set(v => ExternFieldSeparator = v)
                .Converter(new StringConverter());

            Map(m => m.QualitativeSeparator + "")
                .ToAttribute("QualitativeSeparator", false, ";")
                .Set(v => QualitativeSeparator = v)
                .Converter(new StringConverter());

            Map(m => string.IsNullOrWhiteSpace(m.SourceDataset)
                ? null
                : m.SourceDataset)
                .ToAttribute("ExternDataset", false)
                .Set(name => SourceDataset = name)
                .Converter(new StringConverter());

            //Map( m => Owner == null
            //                  ? m.TargetDataset.SWDatabase.Code
            //                  : null )
            //        .ToAttribute( "DatabaseCode", owner == null )
            //        .Set( code => DatabaseCode = code )
            //        .Converter( new StringConverter( ) );

            Map(m => Owner == null
                ? m.TargetDataset.Code
                : null)
                .ToAttribute("DatasetCode", owner == null)
                .Set(code => DatasetCode = code)
                .Converter(new StringConverter());

            Map(m => m.ValueFieldName)
                .ToAttribute("ExternValueFieldName", false, Dataflow.DefaultValueField)
                .Set(v => ExternalValueColumn = v)
                .Converter(new StringConverter());

            Map(GetSourceNames)
                .ToAttribute("ExternFieldNames", false)
                .Set(v => ExternFields = v)
                .Converter(new StringConverter());

            //Map( m => m.PowerFactor )
            //        .ToAttribute( "PowerFactor", false, "1" )
            //        .Set( pf => PowerFactor = pf )
            //        .Converter( new DoubleConverter( ) );

            Map(m => m.DecimalPlaces)
                .ToAttribute("DecimalPlaces", false, "14")
                .Set(pf => DecimalPlaces = pf)
                .Converter(new Int32Converter());

            Map(m => m.RetainTextFormat)
                .ToAttribute("RetainTextFormatting", false, "true")
                .Set(rf => RetainTextFormatting = rf)
                .Converter(new BooleanConverter());

            Map(m => new DimensionPairContainer(m))
                .ToElement(XName.Get("Dimension-Mapping", MappingXmlNamespace), false)
                .Set(fm => DimensionPairs = fm)
                .ClassMap(() => new DimensionPairContainerXMap(this));

            Map(GetValueMappings)
                .ToElement(XName.Get("Value-Mapping", MappingXmlNamespace), false)
                .Set(fm => ValueMappings = fm)
                .ClassMap(() => new ValueMapperXMap(this));

            Map(GetAdvancedMappings)
                .ToElement(XName.Get("Advanced-Dimension-Mapping", MappingXmlNamespace), false)
                .Set(fm => AdvancedMappings = fm)
                .ClassMap(() => new MapperXMap(this));
        }

        private string GetSourceNames(StandardSWMapper m)
        {
            var trg = m.TargetNames.ToIndexer();
            return m.SourceNames.SetEquals(trg, StringComparer.InvariantCulture)
                ? null
                : string.Join(ExternFieldSeparator, m.SourceNames);
        }

        private IEnumerable<string> BuildSourceNames(Dataflow ds)
        {
            if (!string.IsNullOrWhiteSpace(ExternFields))
            {
                var res = ExternFields.Split(ExternFieldSeparator[0]);
                if (!res.Contains(ExternalValueColumn, StringComparer.InvariantCultureIgnoreCase))
                {
                    throw new ApplicationArgumentException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelValueNotInExternField),
                        ExternFields)
                    );
                }
                if (!string.IsNullOrWhiteSpace(ExternalControlCodesColumn) &&
                    !res.Contains(ExternalControlCodesColumn, StringComparer.InvariantCultureIgnoreCase))
                {
                    throw new ApplicationArgumentException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelValueNotInSourceField),
                        ExternFields)
                    );
                }
                return res;
            }
            var srcNames = new IndexedSet<string>();
            if (AdvancedMappings != null)
            {
                srcNames.AddRange(AdvancedMappings.SourceNames);
            }

            srcNames.Add(!string.IsNullOrWhiteSpace(ExternalValueColumn) ? ExternalValueColumn : ds.ValueFieldName);

            if (!string.IsNullOrWhiteSpace(ExternalControlCodesColumn))
            {
                srcNames.Add(ExternalControlCodesColumn);
            }

            srcNames.AddRange(
                DimensionPairs.CoordMappings.Select(cm => cm.ExternDimension ?? cm.Dimension)
                    .Where(n => !string.IsNullOrWhiteSpace(n)));
            srcNames.AddRange(ds.Dimensions.Where(
                dim =>
                    DimensionPairs.CoordMappings.All(cm => !string.Equals(dim.Code, cm.Dimension ?? cm.ExternDimension)))
                .Select(dim => dim.Code));
            return srcNames;
        }


        public override XName Name
        {
            get { return "Mapping"; }
        }

        public Dataflow GetDataset()
        {
            throw new NotImplementedException();

            //SWDataset res = null;
            //try {
            //    if( Owner != null ) {
            //        res = Owner.GetDataset( );
            //    }
            //} catch( ApplicationArgumentException ex) {
            //    //noop
            //}
            //return res ?? SWDatabase.FindDataset( DatabaseCode, DatasetCode );
        }

        private static Mapper GetValueMappings(StandardSWMapper sm)
        {
            var cols = new[]
            {
                "V",
                "CC"
            };
            var mapper = new Mapper(cols, cols, null);
            foreach (var mi in sm.ValueMappings)
            {
                var src = new string[2];
                var prj = new string[2];
                mi.KeySource.CopyTo(src, 0);
                mi.Projection.CopyTo(prj, 0);
                mapper.AddMapping(src, prj, mi.IsRegEx);
            }
            return mapper.MappingItems.Count > 0
                ? mapper
                : null;
        }


        private static Mapper GetAdvancedMappings(StandardSWMapper sm)
        {
            var mapper = new Mapper(sm.SourceNames.ToArray(), sm.TargetNames.ToArray(), sm.MatchingNames,
                sm.TargetTemplate.ToArray());
            foreach (var mi in sm.AdvancedCoordMappings)
            {
                mapper.AddMapping(mi.KeySource, mi.Projection, mi.IsRegEx);
            }

            if (mapper.MappingItems.Count == 0)
            {
                return null;
            }
            return mapper;
        }

        protected override StandardSWMapper Return()
        {
            throw new NotImplementedException();

            //var ds = Owner != null
            //                       ? Owner.GetDataset( )
            //                       : SWDatabase.FindDataset( DatabaseCode, DatasetCode );
            //if( ds == null ) {
            //    throw new ApplicationArgumentException( "The dataset {0}.{1} cannot be found in StatWorks database".F( DatabaseCode, DatasetCode ) );
            //}

            //if (ExternFieldSeparator != null && ExternFieldSeparator.Length != 1)
            //{
            //    throw new ApplicationArgumentException("The field seperator for external fields must be only one character long");
            //}

            //if (QualitativeSeparator != null && QualitativeSeparator.Length != 1)
            //{
            //    throw new ApplicationArgumentException("The qualitative value seperator must be only one character long");
            //}

            ////CheckCoordMappings( ds, ExternFieldSeparator[0] );

            //var res = GetBasicMapper( ds );
            //if (QualitativeSeparator != null)
            //{
            //    res.QualitativeSeparator = QualitativeSeparator[0];
            //}

            //foreach( var vm in DimensionPairs.CoordMappings.Where( vm => vm != null ) ) {
            //    res.AddCoordMapping( vm );
            //}

            //if( ValueMappings != null ) {
            //    foreach( var vm in ValueMappings.MappingItems.Where( vm => vm != null ) ) {
            //        res.AddValueMapping( vm.Projection[ 0 ], vm.Projection[ 1 ], vm.KeySource[ 0 ], vm.KeySource[ 1 ], vm.IsRegEx );
            //    }
            //}

            //if( AdvancedMappings == null ) {
            //    return res;
            //}

            //foreach( var mi in AdvancedMappings.MappingItems.Where( mi => mi != null ) ) {
            //    var ks = new string[ res.SourceCount ];
            //    var pr = new string[ res.TargetCount ];
            //    for( var ii = 0; ii < AdvancedMappings.SourceNames.Count; ii++ ) {
            //        var name = AdvancedMappings.SourceNames[ ii ];
            //        var idx = res.SourceNames.IndexOf( name, StringComparer.InvariantCultureIgnoreCase );
            //        if( idx < 0 ) {
            //            continue;
            //            //throw new ApplicationArgumentException( "The dimension {0} is not defined for the extern dataset {1}".F( name, res.SourceDataset ) );
            //        }
            //        ks[ idx ] = mi.KeySource[ ii ];
            //    }
            //    for( var ii = 0; ii < mi.Projection.Length; ii++ ) {
            //        var name = AdvancedMappings.TargetNames[ ii ];
            //        var idx = res.TargetNames.IndexOf( name, StringComparer.InvariantCultureIgnoreCase );
            //        if( idx < 0 ) {
            //            continue;
            //            //throw new ApplicationArgumentException( "The dimension {0} is not defined for the dataset {1}".F( name, res.TargetDataset.FullCode ) );
            //        }
            //        pr[ idx ] = mi.Projection[ ii ];
            //    }
            //    res.AddMapping( ks, pr, mi.IsRegEx );
            //}

            //return res;
        }

        protected virtual StandardSWMapper GetBasicMapper(Dataflow ds)
        {
            var sourceNames = BuildSourceNames(ds);

            return new StandardSWMapper(ds,
                externColumnNames: sourceNames,
                powerFactor: GetPowerFactor(),
                decimalPlaces: DecimalPlaces,
                retainTextDataFormatting: RetainTextFormatting,
                externDataset: SourceDataset,
                externalValueColumn: ExternalValueColumn,
                externalControlCodesColumn: ExternalControlCodesColumn, name: MapperName);
        }

        protected double GetPowerFactor()
        {
            // ReSharper disable once CompareOfFloatsByEqualityOperator
            double pf = OverridePowerFactor != 0.0
                ? OverridePowerFactor
                : PowerFactor;

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            pf = _ReversePowerFactor && pf != 1.0
                ? 1.0/pf
                : pf;

            return pf;
        }
    }
}
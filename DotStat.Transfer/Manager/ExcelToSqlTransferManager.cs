﻿using System;
using DotStat.Common.Configuration;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Transfer.Consumer;
using DotStat.Transfer.DataflowManager;
using DotStat.Transfer.Exception;
using DotStat.Transfer.Param;
using DotStat.Transfer.Producer;

namespace DotStat.Transfer.Manager
{
    public class ExcelToSqlTransferManager : TransferManager<ExcelToSqlTransferParam>
    {
        public ExcelToSqlTransferManager(BaseConfiguration configuration, IProducer<ExcelToSqlTransferParam> observationProducer, 
            IConsumer<ExcelToSqlTransferParam> observationConsumer) : base(configuration, observationProducer, observationConsumer)
        {
        }

        public override void Transfer(ExcelToSqlTransferParam transferParam)
        {
            if (transferParam == null)
            {
                throw new ArgumentNullException(nameof(transferParam));
            }

            using (Producer)
            {
                var destinationDataflow = Producer.GetDataflow(transferParam);

                if (!Consumer.IsAuthorized(transferParam, destinationDataflow))
                {
                    throw new TransferUnauthorizedException();
                }

                var transferContent = Producer.Process(transferParam, destinationDataflow);

                if (!Consumer.Save(transferParam, destinationDataflow, transferContent))
                {
                    throw new TransferFailedException();
                }
            }

            Log.Notice(LocalizationRepository.GetLocalisedResource(
                    Localization.ResourceId.StreamingFinished,
                    transferParam.CultureInfo.TwoLetterISOLanguageName));
        }
    }
}

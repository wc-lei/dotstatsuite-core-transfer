﻿using DotStat.Common.Configuration;
using DotStat.Transfer.Consumer;
using DotStat.Transfer.Param;
using DotStat.Transfer.Producer;

namespace DotStat.Transfer.Manager
{
    public abstract class TransferManager<T> : ITransferManager<T> where T : TransferParam
    {
        public BaseConfiguration Configuration { get; set; }
        public IProducer<T> Producer { get; set; }
        public IConsumer<T> Consumer { get; set; }

        protected TransferManager(BaseConfiguration configuration, IProducer<T> producer, IConsumer<T> consumer)
        {
            Configuration = configuration;
            Producer = producer;
            Consumer = consumer;
        }

        public abstract void Transfer(T transferParam);
                
    }
}
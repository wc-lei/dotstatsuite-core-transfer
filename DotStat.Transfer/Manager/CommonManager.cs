﻿using System.Linq;
using DotStat.Common.Auth;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Enums;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Db.Exception;
using DotStat.Db.Repository;
using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Transfer.Exception;
using DotStat.Transfer.Param;
using Estat.Sdmxsource.Extension.Constant;
using Newtonsoft.Json.Linq;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;

namespace DotStat.Transfer.Manager
{
    public class CommonManager
    {
        private readonly IMappingStoreDataAccess _mappingStoreDataAccess;
        private readonly IAuthorizationManagement _authorizationManagement;
        private readonly IGeneralConfiguration _generalConfiguration;

        public CommonManager(IMappingStoreDataAccess mappingStoreDataAccess, IAuthorizationManagement authorizationManagement, IGeneralConfiguration generalConfiguration)
        {
            _mappingStoreDataAccess = mappingStoreDataAccess;
            _authorizationManagement = authorizationManagement;
            _generalConfiguration = generalConfiguration;
        }

        public JObject GetPITInfo(ITransferParam transferParam, IDataflowMutableObject sourceDataFlow, ITransactionRepository transactionRepository)
        {
            var dataflow = GetDataflow(transferParam.SourceDataspace.Id, sourceDataFlow);

            if (!IsAuthorized(transferParam, dataflow))
            {
                throw new TransferUnauthorizedException();
            }
            
            return transactionRepository.GetDSDPITInfo(dataflow);
        }

        public void Rollback(ITransferParam transferParam, IDataflowMutableObject sourceDataFlow, IManagementRepository managementRepository, ITransactionRepository transactionRepository)
        {
            var dataflow = GetDataflow(transferParam.SourceDataspace.Id, sourceDataFlow);

            if (!IsAuthorized(transferParam, dataflow))
                throw new TransferUnauthorizedException();
            
            try
            {
                if (!transactionRepository.TryNewTransaction(transferParam.Id, dataflow, TargetVersion.PointInTime, true, transferParam.Principal))
                    throw new TransferFailedException();

                transactionRepository.Rollback(dataflow, _mappingStoreDataAccess);
                managementRepository.CloseTransactionItem(transferParam.Id, true, false);
            }
            catch (System.Exception)
            {
                managementRepository.CloseTransactionItem(transferParam.Id, false, false);
                throw;
            }
        }

        public void Restore(TransferParam transferParam, IDataflowMutableObject sourceDataFlow, IManagementRepository managementRepository, ITransactionRepository transactionRepository)
        {
            var dataflow = GetDataflow(transferParam.SourceDataspace.Id, sourceDataFlow);
            if (!IsAuthorized(transferParam, dataflow))
                throw new TransferUnauthorizedException();

            try
            {
                if (!transactionRepository.TryNewTransaction(transferParam.Id, dataflow, TargetVersion.Live, true, transferParam.Principal))
                    throw new TransferFailedException();

                transactionRepository.Restore(dataflow, _mappingStoreDataAccess);
                managementRepository.CloseTransactionItem(transferParam.Id, true, false);                
            }
            catch (System.Exception)
            {
                managementRepository.CloseTransactionItem(transferParam.Id, false, false);
                throw;
            }
        }

        public bool InitDataDbObjectsOfDataflow(TransferParam transferParam, IDataflowMutableObject targetDataflow, IManagementRepository managementRepository, ITransactionRepository transactionRepository)
        {
            // Check of permission
            if (!_authorizationManagement.IsAuthorized(
                transferParam.Principal,
                transferParam.DestinationDataspace.Id,
                targetDataflow.AgencyId,
                targetDataflow.Id,
                targetDataflow.Version,
                PermissionType.CanImportData))
            {
                // Not authorized to manage data of dataflow
                throw new TransferUnauthorizedException();
            }

            // Check if dataflow exists in mappingstore database
            var dataflow = GetDataflow(transferParam.DestinationDataspace.Id, targetDataflow);

            if (dataflow == null)
            {
                // Dataflow not found in mappingstore database
                throw new ArtefactNotFoundException();
            }

            // Check if dataflow exists in datastore database
            var dataflowId = managementRepository.GetArtefactDbId(dataflow.Base);

            if (dataflowId != -1)
            {
                // Dataflow already present in ARTEFACT table, so initialization has already been done.
                throw new DataflowAlreadyInitializedException();
            }

            try
            {
                if (!transactionRepository.TryNewTransaction(transferParam.Id, dataflow, TargetVersion.Live, false, transferParam.Principal))
                {
                    // Transaction creation attempt failed due to an ongoing concurrent transaction
                    return false;
                }

                var targetTableVersion = managementRepository.GetTargetVersionOfTransaction(transferParam.Id);

                // Closure of transaction. Includes calculation of actual content constraint (that will be empty in this case)
                transactionRepository.CloseTransaction(transferParam.Id, dataflow, _mappingStoreDataAccess,
                    targetTableVersion, false);

                return true;
            }
            catch
            {
                // If there is an unexpected error do the cleanup transaction item before passing the exception
                managementRepository.CloseTransactionItem(transferParam.Id, false, false);

                throw;
            }
        }

        public bool CleanUpDsd(TransferParam transferParam, IDataStructureMutableObject targetDsd,
            IManagementRepository managementRepository, ITransactionRepository transactionRepository)
        {
            // Check of permission
            if (!_authorizationManagement.IsAuthorized(
                transferParam.Principal,
                transferParam.DestinationDataspace.Id,
                targetDsd.AgencyId,
                targetDsd.Id,
                targetDsd.Version,
                PermissionType.CanDeleteStructuralMetadata))
            {
                // Not authorized to manage data of dataflow
                throw new TransferUnauthorizedException();
            }

            var mappingStoreObjects = _mappingStoreDataAccess.GetDsd(
                transferParam.DestinationDataspace.Id,
                targetDsd.AgencyId,
                targetDsd.Id,
                targetDsd.Version);

            if (mappingStoreObjects.DataStructures.Any())
            {
                throw new DatastructureStillExistsException();
            }

            var dsdDbId = managementRepository.GetArtefactDbId(targetDsd.AgencyId, targetDsd.Id,
                new SdmxVersion(targetDsd.Version), SDMXArtefactType.Dsd);

            //If no such DSD
            if (dsdDbId == -1)
            {
                return false;
            }

            try
            {
                if (!transactionRepository.TryNewTransactionForCleanup(transferParam.Id, targetDsd.AgencyId, targetDsd.Id, new SdmxVersion(targetDsd.Version), transferParam.Principal))
                {
                    // Transaction creation attempt failed due to an ongoing concurrent transaction
                    throw new ConcurrentTransactionException();
                }

                // Do the cleanup
                managementRepository.CleanUpDsd(targetDsd.AgencyId, targetDsd.Id, new SdmxVersion(targetDsd.Version));
                managementRepository.CloseTransactionItem(transferParam.Id, true, false);

                return true;
            }
            catch
            {
                // If there is an unexpected error do the cleanup transaction item before passing the exception
                managementRepository.CloseTransactionItem(transferParam.Id, false, false);

                throw;
            }
        }

        private bool IsAuthorized(ITransferParam transferParam, Dataflow dataflow)
        {
            return _authorizationManagement.IsAuthorized(
                transferParam.Principal,
                transferParam.SourceDataspace.Id,
                dataflow.AgencyId,
                dataflow.Base.Id,
                dataflow.Version.ToString(),
                PermissionType.CanImportData
            );
        }

        private Dataflow GetDataflow(string sourceDataSpaceId, IDataflowMutableObject sourceDataflow)
        {
            var dataflow = _mappingStoreDataAccess.GetDataflow(
                sourceDataSpaceId,
                sourceDataflow.AgencyId,
                sourceDataflow.Id,
                sourceDataflow.Version);

            Log.Notice(
                string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DataflowLoaded),
                    dataflow.FullId,
                    sourceDataSpaceId));

            return dataflow;
        }
    }
}

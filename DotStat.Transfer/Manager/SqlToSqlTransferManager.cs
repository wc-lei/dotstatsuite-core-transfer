﻿using System;
using DotStat.Common.Configuration;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Transfer.Consumer;
using DotStat.Transfer.DataflowManager;
using DotStat.Transfer.Exception;
using DotStat.Transfer.Param;
using DotStat.Transfer.Processor;
using DotStat.Transfer.Producer;

namespace DotStat.Transfer.Manager
{
    public class SqlToSqlTransferManager : TransferManager<SqlToSqlTransferParam>
    {
        private readonly ITransferProcessor _transferProcessor;

        public SqlToSqlTransferManager(
            BaseConfiguration configuration, 
            IProducer<SqlToSqlTransferParam> observationProducer, 
            IConsumer<SqlToSqlTransferParam> observationConsumer, 
            ITransferProcessor transferProcessor
        ) : base(configuration, observationProducer, observationConsumer)
        {
            _transferProcessor = transferProcessor;
        }

        public override void Transfer(SqlToSqlTransferParam transferParam)
        {
            if (transferParam == null)
            {
                throw new ArgumentNullException(nameof(transferParam));
            }

            var sourceDataflow = Producer.GetDataflow(transferParam);
            var destinationDataflow = ((IDataflowManager<SqlToSqlTransferParam>)Consumer).GetDataflow(transferParam);

            if (!Producer.IsAuthorized(transferParam, sourceDataflow))
            {
                throw new TransferUnauthorizedException();
            }

            if (!Consumer.IsAuthorized(transferParam, destinationDataflow))
            {
                throw new TransferUnauthorizedException();
            }

            var transferContent = Producer.Process(transferParam, sourceDataflow);

            var verifiedTransferContent = _transferProcessor.Process(sourceDataflow, destinationDataflow, transferContent);

            Log.Notice(
                LocalizationRepository.GetLocalisedResource(
                    Localization.ResourceId.PluginProcessFinished,
                    transferParam.CultureInfo.TwoLetterISOLanguageName));

            if (!Consumer.Save(transferParam, destinationDataflow, verifiedTransferContent))
            {
                throw new TransferFailedException();
            }

            Log.Notice(
                LocalizationRepository.GetLocalisedResource(
                    Localization.ResourceId.StreamingFinished,
                    transferParam.CultureInfo.TwoLetterISOLanguageName));
        }
    }
}

﻿using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;

namespace DotStat.Transfer.Param
{
    public class SdmxFileToSqlTransferParam : TransferParam
    {
        public string FilePath { get; set; }
        public IDataflowMutableObject SourceDataflow { get; set; }
    }
}


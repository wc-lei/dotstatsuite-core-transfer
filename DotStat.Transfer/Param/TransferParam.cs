﻿using System;
using System.Globalization;
using DotStat.Common.Auth;
using DotStat.Common.Configuration.Dto;
using DotStat.Domain;

namespace DotStat.Transfer.Param
{
    public interface ITransferParam
    {
        int Id { get; set; }
        DataspaceInternal SourceDataspace { get; set; }
        DataspaceInternal DestinationDataspace { get; set; }
        DotStatPrincipal Principal { get; set; }
        CultureInfo CultureInfo { get; set; }
        TargetVersion TargetVersion { get; set; }
        DateTime? PITReleaseDate { get; set; }
        Boolean PITRestorationAllowed { get; set; }
        string DataSource { get; set; }
    }

    public class TransferParam : ITransferParam
    {
        public int Id { get; set; }
        public DataspaceInternal SourceDataspace { get; set; }
        public DataspaceInternal DestinationDataspace { get; set; }
        public DotStatPrincipal Principal { get; set; }
        public CultureInfo CultureInfo { get; set; }
        public TargetVersion TargetVersion { get; set; }
        public DateTime? PITReleaseDate { get; set; }
        public Boolean PITRestorationAllowed { get; set; }
        public string DataSource { get; set; }
    }
}
﻿using System.Diagnostics.CodeAnalysis;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;

namespace DotStat.Transfer.Param
{
    public interface ISqlTransferParam : ITransferParam
    {
        IDataflowMutableObject SourceDataflow { get;}
        string SourceQuery { get;}

        IDataflowMutableObject DestinationDataflow { get; set; }
    }

    public class SqlToSqlTransferParam : TransferParam, ISqlTransferParam
    {
        public IDataflowMutableObject SourceDataflow { get; set; }
        public IDataflowMutableObject DestinationDataflow { get; set; }
        public string SourceQuery { get; set; }
    }
}

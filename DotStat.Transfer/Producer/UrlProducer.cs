﻿using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Transfer.Param;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using System.Collections.Generic;
using System.Net.Http;
using Org.Sdmxsource.Sdmx.Api.Engine;
using Org.Sdmxsource.Sdmx.DataParser.Manager;
using Org.Sdmxsource.Util.Io;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using System.Net;
using DotStat.Common.Localization;
using System;

namespace DotStat.Transfer.Producer
{
    public class UrlProducer : IProducer<UrlToSqlTransferParam>
    {
        private readonly ReadableDataLocationFactory _dataLocationFactory;
        private readonly IMappingStoreDataAccess _dataAccess;
        private Dataflow _dataFlow;

        public UrlProducer(IMappingStoreDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
            _dataLocationFactory = new ReadableDataLocationFactory();
        }

        public bool IsAuthorized(UrlToSqlTransferParam transferParam, Dataflow dataflow)
        {
            return true;
        }

        public Dataflow GetDataflow(UrlToSqlTransferParam transferParam)
        {
            if (_dataFlow == null)
            {
                _dataFlow = GetDataflowFromSource(transferParam.Url.ToString(), transferParam.DestinationDataspace.Id);
                if (_dataFlow == null)
                {
                    throw new NotImplementedException(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NoDataflowReferenceInputDataset, transferParam.CultureInfo.TwoLetterISOLanguageName));
                }
            }
            return _dataFlow;
        }

        public TransferContent Process(UrlToSqlTransferParam transferParam, Dataflow dataflow)
        {
            var keyables = new List<IKeyable>();
            var datasetAttributes = new List<IKeyValue>();
            var content = new TransferContent()
            {
                Keyables = keyables,
                DatasetAttributes = datasetAttributes
            };  

            // TODO Internal dataspace
            content.Observations = GetObservations(transferParam.Url.ToString(), dataflow.Dsd, keyables, datasetAttributes);
               
            return content;
        }
        
        private IEnumerable<IObservation> GetObservations(string url, Dsd dsd, List<IKeyable> keyables, List<IKeyValue> datasetAttributes)
        {
            using (var client = new WebClient())
            {
                var task = client.OpenReadTaskAsync(url);
                var waiter = task.GetAwaiter();
                var manager = new DataReaderManager();
                    
                using (var sourceData = _dataLocationFactory.GetReadableDataLocation(waiter.GetResult()))
                {
                    using (var dataReaderEngine = manager.GetDataReaderEngine(sourceData, dsd.Base, null))
                    {
                        foreach (var observation in ReadSource(dataReaderEngine, keyables, datasetAttributes))
                        {
                            yield return observation;
                        }
                    }
                }
            }
        }

        private IEnumerable<IObservation> ReadSource(IDataReaderEngine reader, List<IKeyable> keyables, List<IKeyValue> datasetAttributes)
        {
            keyables.Clear();
            datasetAttributes.Clear();

            if (reader.MoveNextDataset())
            {
                datasetAttributes.AddRange(reader.DatasetAttributes);

                while (reader.MoveNextKeyable())
                {
                    var currentKeyable = reader.CurrentKey;

                    keyables.Add(currentKeyable);

                    if (reader.CurrentKey.Series && reader.MoveNextObservation())
                    {
                        do
                        {
                            var currentObservation = reader.CurrentObservation;

                            yield return currentObservation;
                        }
                        while (reader.MoveNextObservation());
                    }
                }
            }
        }

        private Dataflow GetDataflowFromSource(string url, string dataspace)
        {
            using (var client = new WebClient())
            {
                var task = client.OpenReadTaskAsync(url);
                var waiter = task.GetAwaiter();
                var manager = new DataReaderManager();

                using (var sourceData = _dataLocationFactory.GetReadableDataLocation(waiter.GetResult()))
                {
                    using (var dataReaderEngine = manager.GetDataReaderEngine(sourceData,  _dataAccess.GetRetrievalManager(dataspace)))
                    {
                        if (dataReaderEngine.MoveNextDataset())
                        {
                            //1. Read sdmx file/source to get the dataflow
                            var structureReference = dataReaderEngine.CurrentDatasetHeader.DataStructureReference.StructureReference;
                            var structureType = structureReference.TargetReference.EnumType;
                            if (structureType == Org.Sdmxsource.Sdmx.Api.Constants.SdmxStructureEnumType.Dataflow)
                            {
                                //2. Check that the dataflow exists in the mapping store database
                                return _dataAccess.GetDataflow(dataspace, structureReference.MaintainableReference.AgencyId, structureReference.MaintainableReference.MaintainableId, structureReference.MaintainableReference.Version);
                            }
                        }
                    }
                }
            }
            return null;

        }

        public void Dispose()
        {
            //Nothing to do, IDataReaderEngine is within a "using" clause
        }
    }
}
﻿using System;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using DotStat.Common.Localization;
using DotStat.Domain;
using DotStat.Common.Logger;
using DotStat.MappingStore;
using DotStat.Transfer.DataflowManager;
using DotStat.Transfer.Excel.Excel;
using DotStat.Transfer.Excel.Reader;
using DotStat.Transfer.Exception;
using DotStat.Transfer.Param;
using Org.Sdmxsource.Sdmx.Api.Model.Data;

namespace DotStat.Transfer.Producer
{
    public class ExcelProducer : IProducer<ExcelToSqlTransferParam>
    {
        private readonly IMappingStoreDataAccess _dataAccess;
        private ExcelDataDescription _excelDataDescription;
        private EPPlusExcelDataSource _excelSource;

        public ExcelProducer(IMappingStoreDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }

        public bool IsAuthorized(ExcelToSqlTransferParam transferParam, Dataflow dataflow)
        {
            //As the Source is Excel there is no need to check if the User can read Data.
            return true;
        }

        public Dataflow GetDataflow(ExcelToSqlTransferParam transferParam)
        {
            _excelDataDescription = ExcelDataDescription.Build(
                1,
                string.Empty,
                string.Empty,
                XDocument.Load(new StreamReader(transferParam.EddFilePath)),
                _dataAccess,
                transferParam.DestinationDataspace?.Id);

            var dataflow = _excelDataDescription.Dataflow;

            if (transferParam.DestinationDataspace != null)
                Log.Notice(string.Format(
                        LocalizationRepository.GetLocalisedResource(
                            Localization.ResourceId.DataflowLoaded,
                            transferParam.CultureInfo.TwoLetterISOLanguageName),
                        dataflow.FullId,
                        transferParam.DestinationDataspace.Id));

            return dataflow;
        }

        public TransferContent Process(ExcelToSqlTransferParam transferParam, Dataflow dataflow)
        {
            if (!IsAuthorized(transferParam, dataflow))
            {
                throw new TransferUnauthorizedException();
            }

            _excelSource = new EPPlusExcelDataSource(transferParam.ExcelFilePath);

            var content = new TransferContent
            {
                Observations = new SWCanonicalReader<IObservation>(_excelDataDescription.GetObservationCellIterator(_excelSource), dataflow).AsIEnumerable(),

                DatasetAttributes = _excelDataDescription.DatasetAttributesDescriptor != null
                                        ? new SWCanonicalReader<IKeyValue>(_excelDataDescription.GetDatasetAttributesIterator(_excelSource), dataflow).AsIEnumerable()
                                        : null,

                Keyables    = new SWCanonicalReader<IKeyable>(_excelDataDescription.GetDimAttributesIterator(_excelSource), dataflow).AsIEnumerable(),
            };

            
            return content;
        }

        public void Dispose()
        {
            if (_excelSource != null)
            {
                _excelSource.Dispose();
            }
        }
    }
}
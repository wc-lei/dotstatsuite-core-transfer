﻿using DotStat.Common.Localization;
using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Transfer.Param;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Engine;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.DataParser.Manager;
using Org.Sdmxsource.Util.Io;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;

namespace DotStat.Transfer.Producer
{
    public class SdmxFileProducer : IProducer<SdmxFileToSqlTransferParam>
    {
        private readonly ReadableDataLocationFactory _dataLocationFactory;
        private readonly IMappingStoreDataAccess _dataAccess;
        private Dataflow _dataFlow;

        public SdmxFileProducer(IMappingStoreDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
            _dataLocationFactory = new ReadableDataLocationFactory();
        }

        public bool IsAuthorized(SdmxFileToSqlTransferParam transferParam, Dataflow dataflow)
        {
            return true;
        }

        public Dataflow GetDataflow(SdmxFileToSqlTransferParam transferParam)
        {
            if (_dataFlow != null)
            {
                return _dataFlow;
            }

            _dataFlow = GetDataflowFromSource(transferParam);

            if (_dataFlow == null)
            {
                throw new NotImplementedException(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NoDataflowReferenceInputDataset, transferParam.CultureInfo.TwoLetterISOLanguageName));
            }

            return _dataFlow;
        }

        public TransferContent Process(SdmxFileToSqlTransferParam transferParam, Dataflow dataflow)
        {
            var fileInfo = new FileInfo(transferParam.FilePath);

            if (!fileInfo.Exists)
                throw new ArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.FileNotFound),
                    transferParam.FilePath)
                );

            return Process(fileInfo.FullName, dataflow);
        }

        private TransferContent Process(string filename, Dataflow dataflow)
        {
            var keyables = new List<IKeyable>();
            var datasetAttributes = new List<IKeyValue>();
            
            return new TransferContent()
            {
                Keyables = keyables,
                DatasetAttributes = datasetAttributes,
                Observations = ProcessSdmxFile(filename, dataflow, keyables, datasetAttributes)
            };
        }

        private IEnumerable<IObservation> ProcessSdmxFile(string filename, Dataflow dataflow, List<IKeyable> keyables, List<IKeyValue> datasetAttributes)
        {
            using (var sourceData = _dataLocationFactory.GetReadableDataLocation(new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
            {
                using (var dataReaderEngine = new DataReaderManager().GetDataReaderEngine(sourceData, dataflow.Dsd.Base, null))
                {
                    foreach (var observation in ReadSdmxFile(dataReaderEngine, keyables, datasetAttributes))
                    {
                        yield return observation;
                    }
                }
            }
        }

        private IEnumerable<IObservation> ReadSdmxFile(IDataReaderEngine reader, List<IKeyable> keyables, List<IKeyValue> datasetAttributes)
        {
            keyables.Clear();
            datasetAttributes.Clear();

            if (reader.MoveNextDataset())
            {
                datasetAttributes.AddRange(reader.DatasetAttributes);

                while (reader.MoveNextKeyable())
                {
                    var currentKeyable = reader.CurrentKey;

                    keyables.Add(currentKeyable);

                    if (reader.CurrentKey.Series && reader.MoveNextObservation())
                    {
                        do
                        {
                            var currentObservation = reader.CurrentObservation;

                            yield return currentObservation;
                        }
                        while (reader.MoveNextObservation());
                    }
                }
            }
        }

        private Dataflow GetDataflowFromSource(SdmxFileToSqlTransferParam transferParam)
        {
            var fileInfo = new FileInfo(transferParam.FilePath);

            if (!fileInfo.Exists)
                throw new ArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.FileNotFound),
                    transferParam.FilePath)
                );

            if (transferParam.FilePath.EndsWith(".zip", StringComparison.OrdinalIgnoreCase) ||
                !string.IsNullOrEmpty(transferParam.DataSource) && transferParam.DataSource.EndsWith(".zip", StringComparison.OrdinalIgnoreCase))
            {
                transferParam.FilePath = Path.GetTempFileName();

                using (var zip = ZipFile.OpenRead(fileInfo.FullName))
                {
                    var entry = zip.Entries.First();
                    entry.ExtractToFile(transferParam.FilePath, true);
                }
            }

            return GetDataflowFromSdmxFile(transferParam);
        }

        private Dataflow GetDataflowFromSdmxFile(SdmxFileToSqlTransferParam transferParam)
        {
            using (var sourceData = _dataLocationFactory.GetReadableDataLocation(new FileStream(transferParam.FilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
            {
                using (var dataReaderEngine = new DataReaderManager().GetDataReaderEngine(sourceData, _dataAccess.GetRetrievalManager(transferParam.DestinationDataspace.Id)))
                {
                    if (dataReaderEngine.MoveNextDataset())
                    {
                        //1. Read sdmx file/source to get the dataflow
                        var structureReference = dataReaderEngine.CurrentDatasetHeader.DataStructureReference.StructureReference;

                        if (structureReference.TargetReference.EnumType == SdmxStructureEnumType.Dataflow)
                        {
                            //2. Check that the dataflow exists in the mapping store database
                            return _dataAccess.GetDataflow(transferParam.DestinationDataspace.Id, 
                                structureReference.MaintainableReference.AgencyId, 
                                structureReference.MaintainableReference.MaintainableId, 
                                structureReference.MaintainableReference.Version);
                        }
                    }
                }
            }

            return null;
        }

        public void Dispose()
        {
            //Nothing to do, IDataReaderEngine is within a "using" clause
        }
    }
}
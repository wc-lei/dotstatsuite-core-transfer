﻿
using System.Diagnostics.CodeAnalysis;

namespace DotStat.Transfer.Exception
{
    public class DatastructureStillExistsException : System.Exception
    {
        [ExcludeFromCodeCoverage]
        public DatastructureStillExistsException()
        {
        }

        [ExcludeFromCodeCoverage]
        public DatastructureStillExistsException(string message) : base(message)
        {
        }

        [ExcludeFromCodeCoverage]
        public DatastructureStillExistsException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }
}

﻿
using System.Diagnostics.CodeAnalysis;

namespace DotStat.Transfer.Exception
{
    public class DataflowInitializationFailedException : System.Exception
    {
        [ExcludeFromCodeCoverage]
        public DataflowInitializationFailedException()
        {
        }

        [ExcludeFromCodeCoverage]
        public DataflowInitializationFailedException(string message) : base(message)
        {
        }

        [ExcludeFromCodeCoverage]
        public DataflowInitializationFailedException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }
}
